const db = require("../models");
const User = db.users;
require('dotenv').config();

/**
 * Description: Authentication middleware checking for valid token and injects user info into request
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
const jwt = require("jsonwebtoken");
const {isAdmin} = require("../utilities/authentication");

async function auth(req, res, next) {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const verifiedData = jwt.verify(token, process.env.SECRET);
        if(verifiedData) {
            const condition = {where: {id: verifiedData.id}};
            req.user = await User.findOne(condition);
            req.user.isAdmin = isAdmin(req.user);
            next();
        }
    } catch (e) {
        res.status(401).json({message:"Token expired"});
    }
}

module.exports = auth;
