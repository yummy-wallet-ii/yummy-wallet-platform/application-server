const multer = require('multer');
const fs = require('fs');
const {infoLogger} = require("../services/logger");

/**
 *
 * @type {*|DiskStorage}
 */
const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        checkDirectory(req.query.storeId);
        callBack(null, `uploads/${req.query.storeId}`);
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`);
    }
});

/**
 *
 * @type {DiskStorage}
 */
const categoryStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        checkCustomDirectory('stores');
        checkCustomDirectory(`stores/${req.query.storeId}`);
        checkCustomDirectory(`stores/${req.query.storeId}/categories`);
        callBack(null, `uploads/stores/${req.query.storeId}/categories`);
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`);
    }
});

const productStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        checkCustomDirectory('stores');
        checkCustomDirectory(`stores/${req.query.storeId}`);
        checkCustomDirectory(`stores/${req.query.storeId}/products`);
        callBack(null, `uploads/stores/${req.query.storeId}/products`);
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`);
    }
});

/**
 *
 * @param storeId
 */
function checkCustomDirectory(path) {
    const dir = `./uploads/${path}`;

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

/**
 *
 * @param storeId
 */
function checkDirectory(storeId) {
    const dir = `./uploads/${storeId}`;

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

function checkUploadsDirectory() {
    const dir = `./uploads/`;

    if (!fs.existsSync(dir)) {
        infoLogger.info('Creating uploads directory');
        fs.mkdirSync(dir);
    }
}

const upload = multer({storage: storage});
const uploadCategoryImage = multer({storage: categoryStorage});
const uploadProductImage = multer({storage: productStorage});


module.exports = {
    upload,
    uploadCategoryImage,
    checkUploadsDirectory,
    uploadProductImage
};
