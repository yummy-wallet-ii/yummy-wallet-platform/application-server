const db = require("../models");
const Customer = db.customer;
require('dotenv').config();

/**
 * Description: Authentication middleware checking for valid token and injects user info into request
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
const jwt = require("jsonwebtoken");
const {isAdmin} = require("../utilities/authentication");

async function auth  (req, res, next) {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const verifiedData = jwt.verify(token, process.env.CUSTOMER_SECRET);
        if(verifiedData) {
            req.user = await Customer.findOne({
                raw: true,
                where: {id: verifiedData.id},
                attributes: {
                    exclude: ['baseTransactionId']
                }

            });
            req.user.isAdmin = isAdmin(req.user);
            next();
        }
    } catch (e) {
        res.status(401).json({message:"Token expired"});
    }
}

module.exports = auth;
