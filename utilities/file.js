const fs = require("fs");

/**
 *
 * @param path
 * @param options
 * @returns {Promise<unknown>}
 * @private
 */
function _readFile(path, options) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, options ? options : 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(data);
        });
    })
}

/**
 *
 * @param file
 * @returns {Promise<string>}
 */
async function base64Image_encode(file) {
    const string = await _readFile(file, 'base64');
    return `data:image/jpeg;base64,${string}`;
}

module.exports = {
    base64Image_encode
}
