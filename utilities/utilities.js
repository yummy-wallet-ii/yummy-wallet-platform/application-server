/**
 *
 * @returns {string}
 */
function createdCreditCardNumber() {
    return `${getRandomNumber()}${getRandomNumber()}${getRandomNumber()}${getRandomNumber()} ${getRandomNumber()}${getRandomNumber()}${getRandomNumber()}${getRandomNumber()} ${getRandomNumber()}${getRandomNumber()}${getRandomNumber()}${getRandomNumber()} ${getRandomNumber()}${getRandomNumber()}${getRandomNumber()}${getRandomNumber()}`;
}

/**
 *
 * @returns {number}
 */
function getRandomNumber(){
   return Math.floor(Math.random() * 10);
}

module.exports = {
    createdCreditCardNumber
}
