const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
require('dotenv').config();

/**
 * Description using bcrypt give a plain text password, a hashed one is generated
 * @param password
 * @returns {Promise<null|void|*|NodeJS.Global.Promise>}
 */
async function generateHashedPassword(password) {
    try {
        return await bcrypt.hash(password, 2);
    } catch (e) {
        return null;
    }
}

/**
 * Validating hashed password with bcrypt
 * @param password
 * @param hash
 * @returns {Promise<null|void|*|NodeJS.Global.Promise>}
 */
async function validatePassword(password, hash) {
    try {
        return await bcrypt.compare(password, hash);
    } catch (e) {
        return null;
    }
}

/**
 *
 * @param userId
 * @returns {string}
 */
function signJsonWebToken(userId) {
    return jwt.sign({
        id: userId,
    }, process.env.SECRET, {
        expiresIn: process.env.TOKEN_EXPIRATION
    });
}

/**
 *
 * @param userId
 * @returns {string}
 */
function signCustomerJsonWebToken(userId) {
    return jwt.sign({
        id: userId,
    }, process.env.CUSTOMER_SECRET, {
        expiresIn: process.env.TOKEN_EXPIRATION
    });
}

/**
 *
 * @param user
 * @returns {boolean}
 */
function isAdmin(user) {
    return user.roleId === 1;
}

module.exports = {
    generateHashedPassword,
    validatePassword,
    signJsonWebToken,
    isAdmin,
    signCustomerJsonWebToken
}
