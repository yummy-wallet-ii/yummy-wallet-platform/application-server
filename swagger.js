//swagger.js
const swaggerAutogen = require('swagger-autogen')({ openapi: '3.0.0' })

const doc = {
    info: {
        version: '1.0.0',      // by default: '1.0.0'
        title: 'Application Service',        // by default: 'REST API'
        description: 'Documentation of Yummy Wallet Application Service',  // by default: ''
    },
    servers: [{ url: "http://localhost:9000" }],
}

const outputFile = './endpoints.json'

const endpointsFiles = ['./app.js']

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => console.log('Swagger Completed'));
