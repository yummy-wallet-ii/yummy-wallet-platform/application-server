/**
 * Orders Route
 * @type {{fetchAll: function(*, *, *): Promise<void>, fetchByName: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>, fetchById: function(*, *, *): Promise<void>, remove: function(*, *, *): Promise<void>}|{fetchAll?: function(*, *, *): Promise<void>, fetchById?: function(*, *, *): Promise<void>, fetchByName?: function(*, *, *): Promise<void>, create?: function(*, *, *): Promise<void>, remove?: function(*, *, *): Promise<void>}}
 */
const ordersController = require('../controllers/orders');
const express = require('express');
const router = express.Router();
const customerAuth = require("../middlewares/customerAuth");

router.post('/', customerAuth, ordersController.create);
router.get('/pending/customer', customerAuth, ordersController.fetchCustomerPending);
router.get('/completed/customer', customerAuth, ordersController.fetchCustomerCompleted);

const auth = require("../middlewares/auth");
router.get('/', auth, ordersController.fetchAll);
router.get('/pending', auth, ordersController.fetchAllPending);
router.get('/completed', auth, ordersController.fetchAllCompleted);
router.get('/completed/:storeId', auth, ordersController.fetchAllCompletedByStoreId);
router.get('/:id', auth, ordersController.fetchById);
router.put('/complete/:id', auth, ordersController.complete);

module.exports = router;
