const testController = require('../controllers/test');
const express = require('express');
const router = express.Router();

router.get('/', testController.fetch);
router.post('/', testController.create);

module.exports = router;
