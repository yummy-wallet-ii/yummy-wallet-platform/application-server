const invoicesController = require('../controllers/invoices');
const express = require('express');
const auth = require("../middlewares/auth");
const router = express.Router();

router.get('/', auth, invoicesController.fetchAll);
router.get('/:id', auth, invoicesController.fetchById);
router.get('/export/:id',  invoicesController.exportPDF);
router.post('/', auth, invoicesController.create);


module.exports = router;
