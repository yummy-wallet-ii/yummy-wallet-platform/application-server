/**
 * Users Router
 * @type {{fetchRequestedUser: function(*, *, *): Promise<void>, fetchAll: function(*, *, *): Promise<void>, deleteUser: function(*, *, *): Promise<void>, fetchUserById: function(*, *, *): Promise<void>, updateUser: function(*, *): Promise<void>, externalLogin: function(*, *, *): Promise<void>, login: function(*, *, *): Promise<void>, register: function(*, *, *): Promise<void>, adminLogin: function(*, *, *): Promise<void>}|{deleteUser?: function(*, *, *): Promise<void>, fetchAll?: function(*, *, *): Promise<void>, fetchUserById?: function(*, *, *): Promise<void>, fetchRequestedUser?: function(*, *, *): Promise<void>, login?: function(*, *, *): Promise<void>, register?: function(*, *, *): Promise<void>, updateUser?: function(*, *): Promise<void>, adminLogin?: function(*, *, *): Promise<void>, externalLogin?: function(*, *, *): Promise<void>}}
 */
const userController = require('../controllers/user');
const express = require('express');
const auth = require("../middlewares/auth");
const router = express.Router();

router.get('/', auth,userController.fetchAll);
router.get('/me', auth, userController.fetchRequestedUser);

router.put('/', userController.updateUser);
router.delete('/:id', userController.deleteUser);

router.get('/:id', userController.fetchUserById);


router.post('/login', userController.login);
router.post('/login/admin', userController.adminLogin);
router.post('/externalLogin', userController.externalLogin);
router.post('/register', userController.register);


module.exports = router;
