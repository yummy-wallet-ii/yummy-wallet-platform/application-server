/**
 *
 * @type {{fetchAll: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>, redeem: function(*, *, *): Promise<void>, fetchByCustomerAssigned: function(*, *, *): Promise<void>, remove: function(*, *, *): Promise<void>, assign: function(*, *, *): Promise<void>}|{create?: function(*, *, *): Promise<void>, assign?: function(*, *, *): Promise<void>, redeem?: function(*, *, *): Promise<void>, fetchAll?: function(*, *, *): Promise<void>, fetchByCustomerAssigned?: function(*, *, *): Promise<void>, remove?: function(*, *, *): Promise<void>}}
 */
const couponsController = require('../controllers/coupons');
const express = require('express');
const router = express.Router();

const customerAuth = require("../middlewares/customerAuth");
router.get('/customer', customerAuth, couponsController.fetchByCustomerAssigned);

const auth = require("../middlewares/auth");
router.post('/', auth, couponsController.create);
router.post('/assign', auth, couponsController.assign);
router.post('/redeem', auth, couponsController.redeem);
router.get('/all', auth, couponsController.fetchAll);
router.delete('/:id', couponsController.remove);

module.exports = router;
