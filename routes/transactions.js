/**
 * Transactions Route
 * @type {{fetchByCustomerId: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>, fetchByStoreId: function(*, *, *): Promise<void>}|{create?: function(*, *, *): Promise<void>, fetchByStoreId?: function(*, *, *): Promise<void>, fetchByCustomerId?: function(*, *, *): Promise<void>}}
 */
const transactionsController = require('../controllers/transactions');
const express = require('express');
const router = express.Router();
const auth = require("../middlewares/auth");
const customerAuth = require("../middlewares/customerAuth");

router.get('/customer', customerAuth, transactionsController.fetchByCustomerId);
router.get('/store/:storeId', auth, transactionsController.fetchByStoreId);
router.post('/stripe', customerAuth, transactionsController.stripePayment);
router.post('/init-payment-stripe', transactionsController.stripeInitPayment);
router.post('/', customerAuth, transactionsController.create);

module.exports = router;
