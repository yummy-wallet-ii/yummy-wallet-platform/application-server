const logsController = require('../controllers/logs');
const express = require('express');
const auth = require("../middlewares/auth");
const router = express.Router();

router.get('/notifications', auth, logsController.fetchNotifications);
router.get('/email', auth, logsController.fetchEmails);

module.exports = router;
