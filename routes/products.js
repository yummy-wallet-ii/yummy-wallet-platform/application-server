/**
 * Categories Route
 * @type {{fetchAll: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>}|{fetchAll?: function(*, *, *): Promise<void>, create?: function(*, *, *): Promise<void>}}
 */
const productsController = require('../controllers/products');
const express = require('express');
const router = express.Router();
const {uploadProductImage} = require('../middlewares/fileService');

const customerAuth = require("../middlewares/customerAuth");
router.get('/customer/stamps', customerAuth, productsController.fetchAllCustomerStamps);

const auth = require("../middlewares/auth");
router.get('/', auth, productsController.fetchAll);
router.get('/customer', productsController.fetchAllCustomer);
router.delete('/:id', auth, productsController.deleteProduct);
router.get('/:id', auth, productsController.fetchById);
router.get('/:id/image', productsController.fetchImage);
router.post('/', auth, uploadProductImage.single('file'), productsController.create);
router.post('/updateImage', auth, uploadProductImage.single('file'), productsController.updateImage);
router.put('/', auth, productsController.update);

module.exports = router;
