/**
 *
 * @type {{fetchCreditsByUserId: function(*, *, *): Promise<*|undefined>}|{fetchCreditsByUserId?: function(*, *, *): Promise<*|undefined>}}
 */
const creditsController = require('../controllers/credits');
const express = require('express');
const customerAuth = require("../middlewares/customerAuth");
const router = express.Router();

router.get('/', customerAuth, creditsController.fetchCreditsByUserId);
router.get('/me', customerAuth, creditsController.fetch);
module.exports = router;
