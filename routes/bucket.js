const bucketController = require('../controllers/bucket');
const express = require('express');
const auth = require("../middlewares/auth");
const router = express.Router();

router.get('/', auth, bucketController.fetch);
router.post('/', auth, bucketController.payment);


module.exports = router;
