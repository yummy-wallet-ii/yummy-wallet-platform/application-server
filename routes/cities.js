/**
 * Cities Route
 * @type {{fetchAll: function(*, *, *): Promise<void>, fetchByName: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>, fetchById: function(*, *, *): Promise<void>, remove: function(*, *, *): Promise<void>}|{fetchAll?: function(*, *, *): Promise<void>, fetchById?: function(*, *, *): Promise<void>, fetchByName?: function(*, *, *): Promise<void>, create?: function(*, *, *): Promise<void>, remove?: function(*, *, *): Promise<void>}}
 */
const citiesController = require('../controllers/cities');
const express = require('express');
const router = express.Router();

router.get('/', citiesController.fetchAll);
router.get('/:id', citiesController.fetchById);
router.post('/fetchByName', citiesController.fetchByName);
router.post('/', citiesController.create);
router.delete('/:id', citiesController.remove);

module.exports = router;
