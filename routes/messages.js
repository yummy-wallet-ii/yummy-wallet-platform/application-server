/**
 *
 * @type {{create: function(*, *, *): Promise<*|undefined>}|{create?: function(*, *, *): Promise<*|undefined>}}
 */
const messagesController = require('../controllers/messages');
const express = require('express');
const router = express.Router();

// router.get('/', messagesController.fetchAll);
// router.get('/:id', messagesController.fetchById);
router.post('/', messagesController.create);
router.post('/email', messagesController.sendEmail);
// router.delete('/:id', messagesController.remove);

module.exports = router;
