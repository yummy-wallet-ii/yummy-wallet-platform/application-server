/**
 * Customers Route
 * @type {{addStoreToFavourites: function(*, *, *): Promise<void>, fetchAll: function(*, *, *): Promise<void>, checkIfStoreIsFavorite: function(*, *, *): Promise<void>, me: function(*, *, *): Promise<void>, fetchAllFavoriteStores: function(*, *, *): Promise<void>, fetchByName: function(*, *, *): Promise<void>, fetchById: function(*, *, *): Promise<void>, login: function(*, *, *): Promise<void>, remove: function(*, *, *): Promise<void>, register: function(*, *, *): Promise<void>, removeStoreFromFavourites: function(*, *, *): Promise<void>}|{addStoreToFavourites?: function(*, *, *): Promise<void>, checkIfStoreIsFavorite?: function(*, *, *): Promise<void>, fetchAllFavoriteStores?: function(*, *, *): Promise<void>, fetchAll?: function(*, *, *): Promise<void>, fetchById?: function(*, *, *): Promise<void>, fetchByName?: function(*, *, *): Promise<void>, login?: function(*, *, *): Promise<void>, me?: function(*, *, *): Promise<void>, register?: function(*, *, *): Promise<void>, remove?: function(*, *, *): Promise<void>, removeStoreFromFavourites?: function(*, *, *): Promise<void>}}
 */
const customersController = require('../controllers/customers');
const express = require('express');
const router = express.Router();

router.get('/email', customersController.fetchUserByEmail);
router.get('/universis', customersController.fetchUserInfoByUniversis);
router.get('/:id', customersController.fetchById);
router.post('/fetchByName', customersController.fetchByName);
router.post('/', customersController.register);
router.post('/login', customersController.login);
router.post('/login/external', customersController.externalLogin);
router.delete('/:id', customersController.remove);

const customerAuth = require("../middlewares/customerAuth");
router.get('/me', customerAuth, customersController.me);
router.get('/favourite', customerAuth, customersController.fetchAllFavoriteStores);
router.get('/favourite/:storeId', customerAuth, customersController.checkIfStoreIsFavorite);
router.post('/favourite', customerAuth, customersController.addStoreToFavourites);
router.delete('/favourite/:storeId', customerAuth, customersController.removeStoreFromFavourites);

const auth = require("../middlewares/auth");
router.get('/', auth, customersController.fetchAll);
router.get('/store', auth, customersController.fetchAllByStore);



module.exports = router;
