/**
 * Stores Route
 * @type {{fetchStoreOptions: function(*, *, *): Promise<void>, fetchStoreFrontImageBase64: function(*, *, *): Promise<void>, removeStoreFrontImage: function(*, *, *): Promise<void>, fetchAll: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<*|undefined>, update: function(*, *, *): Promise<void>, fetchById: function(*, *, *): Promise<void>, fetchUserStores: function(*, *, *): Promise<void>, fetchStoreFrontImage: function(*, *, *): Promise<*|undefined>, insertOrUpdateStoreFrontImage: function(*, *, *): Promise<void>, remove: function(*, *, *): Promise<void>}|{create?: function(*, *, *): Promise<*|undefined>, fetchAll?: function(*, *, *): Promise<void>, fetchById?: function(*, *, *): Promise<void>, remove?: function(*, *, *): Promise<void>, fetchUserStores?: function(*, *, *): Promise<void>, update?: function(*, *, *): Promise<void>, insertOrUpdateStoreFrontImage?: function(*, *, *): Promise<void>, fetchStoreFrontImage?: function(*, *, *): Promise<*|undefined>, removeStoreFrontImage?: function(*, *, *): Promise<void>, fetchStoreOptions?: function(*, *, *): Promise<void>, fetchStoreFrontImageBase64?: function(*, *, *): Promise<void>}}
 */
const storesController = require('../controllers/stores');
const express = require('express');
const auth = require("../middlewares/auth");
const router = express.Router();
const {upload} = require('../middlewares/fileService');
const customerAuth = require("../middlewares/customerAuth");

router.get('/', storesController.fetchAll);
router.get('/registered', storesController.fetchRegistered);
router.get('/request', auth, storesController.fetchAllRequested);
router.get('/refills/:storeId', auth, storesController.fetchRefills);
router.post('/refills/:storeId', auth, storesController.createStoreRefill);
router.put('/register', auth, storesController.registerStore);
router.put('/activation', auth, storesController.activationStatusUpdate);
router.get('/userStores', auth, storesController.fetchUserStores);
router.get('/:id', auth, storesController.fetchById);
router.get('/:storeId/logs', auth, storesController.fetchStoreLogs);
router.post('/', storesController.create);
router.put('/:id', auth, storesController.update);
router.put('/customer/:id', customerAuth, storesController.update);
router.delete('/:id', auth, storesController.remove);

router.get('/options/:storeId', auth, storesController.fetchStoreOptions);

router.post('/frontImage', auth, upload.single('file'), storesController.insertOrUpdateStoreFrontImage);
router.get('/frontImage/:storeId', storesController.fetchStoreFrontImage);
router.get('/frontImage/data/:storeId', storesController.fetchStoreFrontImageBase64);
router.delete('/frontImage/:storeId', auth, storesController.removeStoreFrontImage);
router.put('/geofence/:storeId', auth, storesController.updateGeoFence)
module.exports = router;
