const redeemController = require('../controllers/redeem-requests');
const express = require('express');
const router = express.Router();

const customerAuth = require("../middlewares/customerAuth");
router.post('/', customerAuth, redeemController.create);
router.get('/me', customerAuth, redeemController.fetchByCustomerId);


const auth = require("../middlewares/auth");
router.get('/', auth, redeemController.fetch);
router.put('/:id', auth, redeemController.updateRequest);


module.exports = router;
