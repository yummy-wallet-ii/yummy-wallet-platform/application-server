const db = require("../models");
const Categories = db.categories;

async function fetchCategoryById(id) {
    return await Categories.findOne({where: {id}});
}

module.exports = {
    fetchCategoryById
}
