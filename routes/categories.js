/**
 * Categories Route
 * @type {{fetchAll: function(*, *, *): Promise<void>, create: function(*, *, *): Promise<void>}|{fetchAll?: function(*, *, *): Promise<void>, create?: function(*, *, *): Promise<void>}}
 */
const categoriesController = require('../controllers/categories');
const express = require('express');
const auth = require("../middlewares/auth");
const {uploadCategoryImage} = require("../middlewares/fileService");
const router = express.Router();

router.get('/', auth, categoriesController.fetchAll);
router.get('/business', categoriesController.fetchAllBusinessCategories);
router.get('/customer', categoriesController.fetchAllCustomer);
router.get('/fetchCategoryImage/:id', categoriesController.getCategoryImage);
router.post('/', auth, uploadCategoryImage.single('file'), categoriesController.create);
router.post('/updateImage', auth, uploadCategoryImage.single('file'), categoriesController.updateImage);
router.put('/', auth, categoriesController.update);
router.delete('/:id', auth, categoriesController.deleteCategory);

module.exports = router;
