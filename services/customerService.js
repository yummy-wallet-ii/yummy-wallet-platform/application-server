const db = require("../models");
const Customer = db.customer;

/**
 *
 * @param customerId
 * @returns {Promise<*>}
 */
async function fetchCustomerById(customerId) {
    const condition = {where: {id: customerId}};
    const attributes = {
        exclude: ["baseTransactionId"]
    }
  return await Customer.findOne({attributes}, condition);
}

async function fetchCustomerByIdentifier(identifier) {
    const attributes = {
        exclude: ["baseTransactionId"]
    }
    return await Customer.findOne({where: {identifier}, attributes});
}

async function fetchCustomerByToken(token) {
    const attributes = {
        exclude: ["baseTransactionId"]
    }
    return await Customer.findOne({where: {fcmToken: token}, attributes});
}

module.exports = {
    fetchCustomerById,
    fetchCustomerByIdentifier,
    fetchCustomerByToken,
}
