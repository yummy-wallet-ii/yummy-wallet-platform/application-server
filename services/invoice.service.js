const db = require("../models");
const Invoice = db.invoices;
const Category = db.categories;
const {fetchStoreById} = require("./store.service");
const moment = require("moment/moment");

async function fetchInvoiceById(id) {
    const invoice = await Invoice.findOne({where: {id}});
    if(!invoice) {return null;}

    const store = await fetchStoreById(invoice.store_id)
    const categoryCondition = {where: {id: store.categoryId}}
    const category = await Category.findOne(categoryCondition);
    const model = {
        id: invoice.id,
        store,
        category: category.description,
        price: invoice.price,
        price_tax: invoice.price * 0.024,
        dateCreated: moment(invoice.dateCreated).format('DD-MM-YYYY HH:mm')
    }
    model.final_price = model.price + model.price_tax;

    return model;
}

module.exports = {fetchInvoiceById}
