const nodemailer = require('nodemailer');

async function sendEmailToRecipients(message, subject, to, attachments, sender) {
    try {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: 'yssDevelopmentSentry@gmail.com',
                pass: 'opggfcwgiksvrgog'
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        let from;
        if (sender) {
            from = sender;
        } else {
            from = 'yssDevelopmentSentry@gmail.com'
        }
        const mailOptions = {
            from,
            to: to,
            subject: subject,
            html: message,
            attachments: attachments && attachments.hasOwnProperty('files') ? attachments.files : []
        };

       return await transporter.sendMail(mailOptions)
    } catch (e) {
       throw e;
    }
}

module.exports = {sendEmailToRecipients}
