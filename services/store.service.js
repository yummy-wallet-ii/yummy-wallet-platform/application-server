const db = require("../models");
const {QueryTypes} = require("@sequelize/core");
const moment = require("moment");
const StoreOptions = db.storeOptions;
const StoreRefills = db.storeRefill;
const Store = db.stores;

/**
 *
 * @param storeId
 * @returns {Promise<*|null>}
 */
async function fetchStoreById(storeId) {
    const stores = await db.sequelize.query(`select * from user_stores_data where id = ${storeId}`, {
        logging: console.log,
        type: QueryTypes.SELECT,
        plain: false,
        raw: true
    });

    return stores.length > 0 ? stores[0] : null;
}

async function completeRefill(refill) {
    await StoreRefills.create(refill);
}

async function updateStoreBucket(storeId, amount) {
    const store = await fetchStoreById(storeId);
    if (store) {
        let bucket = store.bucket + amount;
        await Store.update(
            {bucket},
            {where: {id: storeId}}
        )
    }
}

async function fetchStoreRefills(storeId) {
    const condition = {where: {storeId}};
    const data = await StoreRefills.findAll(condition);
    data.forEach(record => {
        record.transactionDate = moment(record.transactionDate).format('MM-DD-YYYY HH:mm')
    })
    return data;
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchAllStores(isActive = 0) {
    const data = await db.sequelize.query(`select * from stores_data where isActive = ${isActive}`, {
        logging: console.log,
        type: QueryTypes.SELECT,
        plain: false,
        raw: true
    });

    for (const record of data) {
        if (!isActive) {
            record.requestDate = moment(record.createdAt).format('DD-MM-YYYY');
        }
        record.createdAt = moment(record.createdAt).format('DD-MM-YYYY');
    }
    return data;
}

async function fetchRegisteredStores() {
    const data = await db.sequelize.query(`select * from stores_data where isRegistered = 1`, {
        logging: console.log,
        type: QueryTypes.SELECT,
        plain: false,
        raw: true
    });

    for (const record of data) {
        record.createdAt = moment(record.createdAt).format('DD-MM-YYYY');
    }
    return data;
}

async function fetchAllUnregisteredStores() {
    const data = await db.sequelize.query(`select * from stores_data where isRegistered = 0`, {
        logging: console.log,
        type: QueryTypes.SELECT,
        plain: false,
        raw: true
    });

    for (const record of data) {
        record.requestDate = moment(record.createdAt).format('MM-DD-YYYY');
        record.createdAt = moment(record.createdAt).format('DD-MM-YYYY');
    }
    return data;
}

/**
 *
 * @param storeId
 * @returns {Promise<*>}
 */
async function fetchStoreOptionsByStoreId(storeId) {
    const condition = {where: {storeId}};
    return await StoreOptions.findOne(condition);
}


module.exports = {
    fetchStoreById,
    fetchAllStores,
    fetchAllUnregisteredStores,
    fetchStoreOptionsByStoreId,
    fetchStoreRefills,
    completeRefill,
    fetchRegisteredStores,
    updateStoreBucket
}
