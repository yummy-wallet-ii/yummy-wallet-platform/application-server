const db = require("../models");
const Settings = db.settings;

/**
 *
 * @returns {Promise<*|{type: *}|{type: *}|null>}
 */
async function fetchCreditBalancer() {
    const condition = {where: {id: 1}};
    const settings = await Settings.findOne(condition);
    return settings ? settings.credit_balancer : null;
}

module.exports = {
    fetchCreditBalancer
}
