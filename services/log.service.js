const db = require("../models");
const LogTypes = db.logTypes;
const Actions = db.actions;
const Logs = db.logs;
const EmailLogs = db.emailLogs;
const NotificationLogs = db.notificationLogs;

/**
 * Description Initialize the logTypes table with data - execute ONLY if table is EMPTY
 * @returns {Promise<void>}
 */
async function initializeLogTypes() {
        const types = [
            {description: 'Email'},
            {description: 'SMS'},
            {description: 'Transaction'},
            {description: 'Category'},
            {description: 'Product'},
            {description: 'Notification'}
        ];
        for (const type of types) {
            await LogTypes.create(type);
        }
        console.log('Log Types Created Successfully');
}

/**
 * Description Initialize the actions table with data - execute ONLY if table is EMPTY
 * @returns {Promise<void>}
 */
async function initializeActions() {
        const actions = [
            {description: 'Create'},
            {description: 'Edit'},
            {description: 'Delete'},
            {description: 'Send'},
            {description: 'Complete'}
        ];
        for (const action of actions) {
            await Actions.create(action);
        }
        console.log('Log Types Created Successfully');
  }

/**
 *
 * @param log
 * @returns {Promise<void>}
 * @constructor
 */
async function Logging(log) {
    await Logs.create(log);
}

async function logSentEmail(email) {
    await EmailLogs.create(email);
}

async function logSentNotification(notification) {
    await NotificationLogs.create(notification);
}

module.exports = {
    initializeLogTypes,
    initializeActions,
    Logging,
    logSentEmail,
    logSentNotification
}
