const fs = require('fs');
const path = require('path');
const pdfmake = require('pdfmake');

const line = '<svg height="20" width="515">\n' +
    '  <line x1="0" y1="10" x2="515" y2="10" style="stroke:rgb(168,168,168);stroke-width:1" />\n' +
    '</svg>';


function addLine(documentStructure) {
    documentStructure.content.push({svg: line});
    return documentStructure;
}

function addSpace(documentStructure) {
    documentStructure.content.push({
        columns: [
            {
                style: 'space',
                text: ``
            },
        ]
    });
    return documentStructure;
}
function addSpaceSmall(documentStructure) {
    documentStructure.content.push({
        columns: [
            {
                style: 'spaceSm',
                text: ``
            },
        ]
    });
    return documentStructure;
}

function addSpaceRemove(documentStructure) {
    documentStructure.content.push({
        columns: [
            {
                style: 'spaceRm',
                text: ``
            },
        ]
    });
    return documentStructure;
}

function addHeader(documentStructure, value) {
    documentStructure.content.push({
        columns: [
            {
                style: 'header',
                text: `${getUTF8String(value)}`
            }
        ]
    });
    return documentStructure;
}

function addValue(documentStructure, title, value) {
    documentStructure.content.push({
        columns: [
            {
                style: 'label',
                text: `${getUTF8String(title)}`,
                width: '15%'
            },
            {
                style: 'value',
                text: getUTF8String(value)
            }
        ]
    });
    return documentStructure;
}

function addRowLabel(documentStructure, value) {
    documentStructure.content.push({
        columns: [
            {
                style: 'rowLabel',
                text: `${getUTF8String(value)}`
            }
        ]
    });
    return documentStructure;
}

function initializePrinter() {
    const fonts = {
        CenturyGothic: {
            normal: path.join(__dirname, '../assets/fonts/Century Gothic.ttf'),
            bold: path.join(__dirname, '../assets/fonts/Century Gothic Bold.ttf'),
            italics: path.join(__dirname, '../assets/fonts/gothici.ttf'),
            bolditalics: path.join(__dirname, '../assets/fonts/CGOTHICBI.ttf')
        }
    };
    return new pdfmake(fonts);
}

async function downloadPDF(documentStructure) {
    try {
        return new Promise((resolve, reject) => {
            try {
                const printer = initializePrinter();
                const pdfDocument = printer.createPdfKitDocument(documentStructure, {});
                let chunks = [];
                pdfDocument.on('data', (chunk) => {
                    chunks.push(chunk);
                });
                pdfDocument.on('end', () => {
                    try {
                        const result = Buffer.concat(chunks);
                        return resolve(result.toString('base64'));
                    } catch (error) {
                        return reject(error);
                    }
                });
                pdfDocument.end();
            } catch (error) {
                return reject(error);
            }
        });
    } catch (error) {
        throw error;
    }
}

function initializeDocumentStructure() {
    return {
        content: [
            {
                image: `data:image;base64,${
                    fs.readFileSync(path.join(__dirname, '../assets/images/logo.png')).toString('base64')
                }`,
                style: 'logo',
                width: 50
            }
        ],
        defaultStyle: {
            font: 'CenturyGothic',
        },
        styles: {
            label: {
                bold: true,
                fontSize: 10,
                width: 300,
                fontWeight: 600,
            },
            value: {
                fontSize: 10,
            },
            logo: {
                alignment: 'center',
                margin: [0, -10, 0, 10]
            },
            header: {
                bold: false,
                color: '#08044c',
                fontSize: 12,
                alignment: 'center',
                margin: [0, 0, 0, 5]
            },
            rowLabel: {
                color: '#08044c',
                fontSize: 12,
                bold: false,
                margin: [0, 0, 0, -5]
            },
            spaceRm: {
                margin: [0, 0, 0, -10]
            },
            space: {
                // height: 20,
                margin: [0, 0, 0, 2]
            },
        }
    };
}

function getUTF8String(text) {
    const utf8Buffer = Buffer.from(text, 'utf-8');
    return  utf8Buffer.toString('utf-8');
}

module.exports = {
    addLine,
    addValue,
    initializeDocumentStructure,
    downloadPDF,
    addHeader,
    addSpace,
    addRowLabel,
    addSpaceSmall,
    addSpaceRemove
}
