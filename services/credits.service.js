const db = require("../models");
const {QueryTypes} = require("@sequelize/core");
const Credits = db.credits;
/**
 *
 * @param cashBackPercentage
 * @param amount
 * @param creditBalancer
 * @returns {Promise<number>}
 */
async function generateCredits(cashBackPercentage, amount, creditBalancer) {
    const cashBack = cashBackPercentage * amount;

    // return generated Credits
    return cashBack * creditBalancer;
}

/**
 *
 * @param userId
 * @returns {Promise<number>}
 */
async function fetchTotalCreditsByUserId(userId) {
    const transactionsQuery = `select * from transactions_data where customerId = ${userId}`;
    const transactions = await db.sequelize.query(transactionsQuery, {
        logging: console.log,
        type: QueryTypes.SELECT,
        plain: false,
        raw: true
    });

    let credits = 0;
    transactions.forEach(transaction => {
        credits += transaction.credits;
    });

    return credits;
}

async function fetchByCreditsCustomerId(userId) {
   return await Credits.findOne({where:{userId}});
}

async function redeemCredits(userId, credits) {
    const balance = await Credits.findOne({where:{userId}});
    const currentCredits = balance.credits;
    const finalCredits = currentCredits - credits;

    await Credits.update(
        { credits: finalCredits },
        { where: { userId} }
    )
}

async function updateCustomerCreditsBalance(userId, credits=null) {
    const balance = await Credits.findOne({where:{userId}});
    if(balance) {
        const currentCredits = balance.credits;
        const finalCredits = currentCredits + credits;
        await Credits.update(
            { credits: finalCredits },
            { where: { userId} }
        )
    } else {
        const creditsModel = {
            userId,
            credits
        }
        await Credits.create(creditsModel);
    }
}

module.exports = {
    generateCredits,
    fetchTotalCreditsByUserId,
    fetchByCreditsCustomerId,
    updateCustomerCreditsBalance,
    redeemCredits
}
