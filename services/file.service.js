const fs = require('fs');
const os = require('os');

/**
 *
 * @param path
 * @returns {Promise<unknown>}
 */
async function getImageBase64FromFile(path) {
    return new Promise((resolve,reject) => {
        try {
            if(os.platform() !== 'win32') {
                path = path.replace(/\\/g, "/");
            }
            const image = fs.readFileSync(path);
            resolve(new Buffer.from(image).toString('base64'));
        } catch (e) {
            reject(e.message);
        }
    });
}

module.exports = {
    getImageBase64FromFile
}
