const {createLogger, format, transports} = require('winston');

/**
 * Default Error Logger
 * @type {winston.Logger}
 */
const errorLogger = createLogger({
    level: 'error',
    transports: [
        new transports.File({
            filename: './logs/error.log',
            format: format.combine(
                format.timestamp({format: 'MMM-DD-YYYY HH:mm:ss'}),
                format.align(),
                format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`)
            ),
            level: 'error'
        })
    ]
});

/**
 * Info Logger
 * @type {winston.Logger}
 */
const infoLogger = createLogger({
    level: 'info',
    transports: [
        new transports.File({
            filename: './logs/info.log',
            format: format.combine(
                format.timestamp({format: 'MMM-DD-YYYY HH:mm:ss'}),
                format.align(),
                format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`)
            ),
            level: 'info'
        })
    ]
});

/**
 * Unhandled Exceptions logger
 * @type {winston.Logger}
 */
const unhandledErrorLogger = createLogger({
    level: 'error',
    transports: [
        new transports.File({
            filename: './logs/unhandled-error.log',
            format: format.combine(
                format.timestamp({format: 'MMM-DD-YYYY HH:mm:ss'}),
                format.align(),
                format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`)
            ),
            level: 'error'
        })
    ]
});

module.exports = {errorLogger, infoLogger, unhandledErrorLogger};
