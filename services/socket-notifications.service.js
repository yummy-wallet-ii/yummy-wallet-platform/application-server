/**
 *
 * @param topic
 * @param storeId
 * @param message
 * @param socket
 * @returns {Promise<void>}
 */
async function sendSocketNotification(topic, storeId, message, socket) {
    try {
        socket.emit(`${topic}-${storeId}`, message);
    } catch (e) {
        console.log(e.message);
    }
}

module.exports = {
    sendSocketNotification
}
