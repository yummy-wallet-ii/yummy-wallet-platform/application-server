const nodemailer = require('nodemailer');

/**
 *
 * @param params
 * @returns {Promise<void>}
 */
async function sendEmail(params) {
    try {
        const {message, subject, to, attachment, sender} = params;
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: 'yssDevelopmentSentry@gmail.com',
                pass: 'opggfcwgiksvrgog'
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        const attachments = [];
        if (attachment) {
            attachments.push({filename: 'report.pdf', content: attachment});
        }

        const mailOptions = {
            from: sender,
            to: to,
            subject: subject,
            html: message,
            attachments
        };

        // const sendRequest = await transporter.sendMail(mailOptions);
        await transporter.sendMail(mailOptions);
        console.log('Email sent successfully');
        // const model = {
        //     messageId: sendRequest.messageId,
        //     accepted: sendRequest.accepted.length > 0 ? sendRequest.accepted[0] : null,
        //     rejected: sendRequest.rejected.length > 0 ? sendRequest.rejected[0] : null,
        //     response: sendRequest.response,
        //     subject: subject,
        //     envelope: JSON.stringify(sendRequest.envelope),
        //     action: 1,
        //     action_date: formatDate(new Date())
        // };

        // await LogEmail(model);


    } catch (e) {
        console.log(e);
        // const model = {
        //     messageId: e.message,
        //     accepted: null,
        //     rejected: to,
        //     subject: subject,
        //     response: e.response,
        //     envelope: '',
        //     action: 0,
        //     action_date: formatDate(new Date())
        // };
        //
        // await LogEmail(model);
    }
}

module.exports = {
    sendEmail
}
