const {fetchStoreOptionsByStoreId} = require("./store.service");
const {fetchCreditBalancer} = require("./settings.service");
const db = require("../models");
const {generateCredits, updateCustomerCreditsBalance} = require("./credits.service");
const {postToBlockChain, BlockChainEndpoints} = require("./blockchain.service");
const Transaction = db.transaction;


/**
 *
 * @param transaction
 * @returns {Promise<void>}
 */
async function createTransaction(transaction, order) {
    transaction.userId = transaction.id;
    transaction.credit_balancer = await fetchCreditBalancer();
    const storeOptions = await fetchStoreOptionsByStoreId(transaction.storeId);

    transaction.credits = await generateCredits(storeOptions.cashbackPercentage, transaction.price, transaction.credit_balancer);

    const blockChainBody = {
        storeId: transaction.storeId,
        customerId: transaction.customerId,
        price: +transaction.price,
        coins: transaction.credits,
        items: order.items.map(item => item.id),
        coupons: order.coupons ? order.coupons : [],
        transactionDate: new Date().getTime(),
        cashback: storeOptions.cashbackPercentage,
        actionId: 1
    }
    const {status, message, data } = await postToBlockChain(BlockChainEndpoints.CustomerTransaction, blockChainBody);
    if (status === 200) {
        transaction.blockChainTransactionId = data.id;
    }


    await Transaction.create(transaction);
    await updateCustomerCreditsBalance(transaction.customerId, transaction.credits);
}

module.exports = {
    createTransaction
}
