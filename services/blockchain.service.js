// Define your client ID and client secret
const axios = require("axios");
require('dotenv').config();

const blockChainApi = process.env.BLOCKCHAIN_API;
const options = {
    headers: {
        'x_client_id': process.env.CLIENT_ID,
        'x_client_secret': process.env.CLIENT_SECRET
    }
}

async function fetchFromBlockChain(endpoint) {
    try {
        const url = `${blockChainApi}/${endpoint}`;
        const request = {
            method: 'GET',
            headers: {'x_client_id': `${process.env.CLIENT_ID}`, 'x_client_secret': `${process.env.CLIENT_SECRET}`},
            url,
        };

        const response = await axios(request);
        return response.data;
    } catch (e) {
        throw e;
    }
}

async function postToBlockChain(endpoint, body) {
    try {
        const url = `${blockChainApi}/${endpoint}`;
        const request = {
            method: 'POST',
            headers: {'x_client_id': `${process.env.CLIENT_ID}`, 'x_client_secret': `${process.env.CLIENT_SECRET}`},
            data: body,
            url,
        };

        const response = await axios(request);
        return response.data;
    } catch (e) {
        throw e;
    }
}

const BlockChainEndpoints = {
    CustomerTransaction: 'customer-transaction',
    RedeemCoins: 'redeem-coins',
    YSOFTPayment: 'yshoft-payment',
    StorePayment: 'store-payment'
}

module.exports = {
    BlockChainEndpoints,
    fetchFromBlockChain,
    postToBlockChain
}
