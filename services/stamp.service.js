const db = require("../models");
const {fetchCategoryById} = require("../routes/category.service");
const CustomerStamps = db.customerStamps;

async function fetchCustomerStamps(categoryId, customerId) {
    const condition = {where: {categoryId, customerId}}
    return await CustomerStamps.findOne(condition);
}

async function updateCustomerStamps(categoryId, customerId, stamps, category) {
    const currentStamps = await fetchCustomerStamps(categoryId, customerId);
    if (currentStamps) {
        let cStamps = currentStamps.stamps;
        const stampsLimit = category.stampsRewards;
        if (cStamps >= stampsLimit) {
            cStamps = 0;
            cStamps++
        }
        await CustomerStamps.update({
            stamps: cStamps
        }, {where: {categoryId, customerId}})
    } else {
        const model = {
            categoryId,
            customerId,
            stamps
        }
        await CustomerStamps.create(model);
    }
}

module.exports = {fetchCustomerStamps, updateCustomerStamps}
