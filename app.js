const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const environmentDevelopment = require('./environments/development');
const environmentProduction = require('./environments/production');
const environmentStaging = require('./environments/staging');
const helmet = require('helmet');
const http = require('http');
require('dotenv').config();
const {checkUploadsDirectory} = require("./middlewares/fileService");
const { Server } = require('socket.io');
const Sentry = require("@sentry/node");
require("@sentry/tracing");
const rfs = require('rotating-file-stream');
const path = require('path');

const {errorLogger, infoLogger, unhandledErrorLogger} = require('./services/logger');

//#region Initializing Basic Config and Basic app uses

rfs.createStream('access.log', {
    compress: 'gzip',
    interval: '1d',
    path: path.join(__dirname, 'logs')
});

const isDevelopment = process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'staging';
if (!isDevelopment) {
    global.config = process.env.NODE_ENV === 'production' ? environmentProduction : environmentStaging;
} else {
    global.config = environmentDevelopment;

    const morgan = require('morgan');

    app.use(morgan('dev'));
    // Initialize Sentry
    Sentry.init({
        dsn: "SentryID",
        integrations: [
            // enable HTTP calls tracing
            new Sentry.Integrations.Http({ tracing: true }),
        ],
        tracesSampleRate: 1.0,
    });
}
if (global.config.shouldSyncDatabase) {
    const db = require("./models");
    db.sequelize.sync();
}

// The request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.use(helmet());

app.use(bodyParser.json({
    limit: '1mb'
}));

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '1mb'
}));
//#endregion

//#region Initializing Server
const server = http.createServer(app).listen(global.config.server.port, global.config.server.host, () => {
    console.log(`HTTP is listening in PORT = ${global.config.server.port}.`);
    infoLogger.info(`Server is listening in port ${global.config.server.port}!`);
});

const io = new Server(server);
const ioMessage = 'Socket up and running';
console.log(ioMessage);
infoLogger.info(ioMessage);

io.on('connection', socket => {
    console.log('a user connected');
    app.set('socket', socket);
});
//#endregion

checkUploadsDirectory();
//region Initializing Routes
const bucketRoutes = require('./routes/bucket');
const categoriesRoute = require('./routes/categories');
const citiesRoute = require('./routes/cities');
const creditsRoute = require('./routes/credits');
const customersRoute = require('./routes/customers');
const invoicesRoute = require('./routes/invoices');
const messagesRoute = require('./routes/messages');
const ordersRoute = require('./routes/orders');
const productsRoute = require('./routes/products');
const storesRoute = require('./routes/stores');
const transactionsRoute = require('./routes/transactions');
const userRoute = require('./routes/user');
const logsRoute = require('./routes/logs');
const redeemRequestsRoute = require('./routes/redeem-requests');
const {initializeFirebaseService} = require("./firebase/push-notifications");

//#endregion

//region Using CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type, sentry-trace');

    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    next();
});
//#endregion

//#region Using Routes
app.use('/api/v1/categories', categoriesRoute // #swagger.tags = ['Categories']
);
app.use('/api/v1/bucket', bucketRoutes // #swagger.tags = ['Bucket']
);
app.use('/api/v1/users', userRoute // #swagger.tags = ['Users']
);
app.use('/api/v1/cities', citiesRoute // #swagger.tags = ['Cities']
);
app.use('/api/v1/customers', customersRoute // #swagger.tags = ['Customers']
);
app.use('/api/v1/logs', logsRoute // #swagger.tags = ['Logs']
);
app.use('/api/v1/messages', messagesRoute // #swagger.tags = ['Messages']
);
app.use('/api/v1/stores', storesRoute // #swagger.tags = ['Stores']
);
app.use('/api/v1/transactions', transactionsRoute // #swagger.tags = ['Transactions']
);
app.use('/api/v1/credits', creditsRoute // #swagger.tags = ['Credits']
);
app.use('/api/v1/products', productsRoute // #swagger.tags = ['Products']
);
app.use('/api/v1/orders', ordersRoute // #swagger.tags = ['Orders']
);
app.use('/api/v1/invoices', invoicesRoute // #swagger.tags = ['Invoices']
);

app.use('/api/v1/redeem', redeemRequestsRoute // #swagger.tags = ['Invoices']
);
//#endregion

//#region Initialize Firebase
try {
    initializeFirebaseService();
    // initializeLogTypes();
    // initializeActions();
} catch (e) {
    console.log(e.message);
}
//#endregion

app.use(Sentry.Handlers.errorHandler());

//#region Error handling && Not Found
app.use((req, res) => {
    console.log(req.error);
    if(req.error) {
        errorLogger.error(req.error.message);
    }
    if (req.error.sqlState) {
        return res.status(400).send('Error at database.');
    } else {
        return res.status(406).send(`Error at process: ${req.error.message}`);
    }
});

app.use((req, res) => {
    unhandledErrorLogger.error('Not found.');
    return res.status(404).send('Not found.');
});
//#endregion
