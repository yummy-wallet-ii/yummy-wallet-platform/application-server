const { initializeApp, cert } = require('firebase-admin/app');

const { messaging } = require('firebase-admin');

const serviceAccount = require('./yummy-wallet-dev-firebase.json');
const {infoLogger} = require("../services/logger");

/**
 * Initialize Firebase with giver credentials
 */
function initializeFirebaseService() {
    initializeApp({credential: cert(serviceAccount)});
    const infoMessage = 'Firebase initialized successfully';
    console.log(infoMessage);
    infoLogger.info(infoMessage);
}

/**
 *
 * @param tokens array of firebase Tokens
 * @param title
 * @param body
 * @returns {Promise<void>}
 */
async function sendNotifications(tokens, title, body) {
    const message = {
        tokens: tokens,
        notification: {
            title: title,
            body: body,
        },
        android: {
            notification: {
                defaultSound: true,
                defaultVibrateTimings: true,
                priority: 'high',
                channelId: 'notifee'
            },
        },
    };
    const test = await messaging().sendMulticast(message);
    console.log(test);
}

module.exports = {
    initializeFirebaseService,
    sendNotifications
}
