require('dotenv').config();

module.exports = {
  shouldSyncDatabase: true,
  server: {
    host: '0.0.0.0',
    port: 9000
  },
  database: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    server: process.env.DB_HOST,
    database: process.env.DATABASE,
    port: 3306,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
};
