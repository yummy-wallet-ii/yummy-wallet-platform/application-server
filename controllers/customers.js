const db = require("../models");
const Customer = db.customer;
const Favourite = db.favourite;
const Store = db.stores;
const StoreToUser = db.storeToUser;
const Transaction = db.transaction;

const Sequelize = require('sequelize');
const {generateHashedPassword, validatePassword, signCustomerJsonWebToken} = require("../utilities/authentication");
const {createdCreditCardNumber} = require("../utilities/utilities");
const axios = require("axios");
const moment = require("moment");
const sequelize = require("sequelize");
const Op = Sequelize.Op;

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function register(req, res, next) {
    try {
        const customer = req.body;
        const hashedPassword = await generateHashedPassword(customer.password);
        customer.password = hashedPassword;
        customer.roleId = 3;
        customer.creditCardNumber = createdCreditCardNumber();
        await Customer.create(customer);
        res.status(200).json('Completed');
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function login(req, res, next) {
    try {
        const customer = req.body;

        const currentCustomer = await Customer.findOne({
            raw: true,
            where: {identifier: customer.identifier},
            attributes: {
                exclude: ['baseTransactionId']
            }
        });

        if(!currentCustomer) {
            return res.status(404).json({message:'O χρήστης δεν υπάρχει!'});
        }

        const match = await validatePassword(customer.password, currentCustomer.password);
        if (match) {
            await Customer.update(
                { fcmToken: customer.fcmToken },
                { where: { id: currentCustomer.id } }
            )
            const token = signCustomerJsonWebToken(currentCustomer.id);
            res.status(200).json(token);
        } else {
            res.status(404).json('User not found');
        }
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

async function externalLogin(req, res, next) {
    try {
        const customer = req.body;

        const currentCustomer = await Customer.findOne({
            raw: true,
            where: {identifier: customer.identifier},
            attributes: {
                exclude: ['baseTransactionId']
            }
        });

        if(!currentCustomer) {
            return res.status(404).json({message:'O χρήστης δεν υπάρχει!'});
        }

        await Customer.update(
            { fcmToken: customer.fcmToken },
            { where: { id: currentCustomer.id } }
        )
        const token = signCustomerJsonWebToken(currentCustomer.id);
        res.status(200).json(token);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        const activeCustomers = [];
        const customers = await Customer.findAll(
            {
                attributes: {
                    include: [[sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y"), "createdAt"]],
                    exclude: ["baseTransactionId", "password"],
                }
            }
        )
        customers.forEach(c => {
           activeCustomers.push(c);
        })
        console.log(activeCustomers);
        res.status(200).json(activeCustomers);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllByStore(req, res, next) {
    try {
        const condition = {where: {userId: +req.user.id}}
        const store = await StoreToUser.findOne(condition);

        const transactions = await Transaction.findAll({
            where: {storeId: store.storeId},
            attributes: [
                [Sequelize.fn('DISTINCT', Sequelize.col('customerId')) ,'customerId'],
            ]
        });

        const customers = [];
        for(const transaction of transactions) {
            const customer = await Customer.findOne(
                {
                    where: {id: transaction.customerId},
                    attributes: {
                        exclude:["baseTransactionId"]
                    },

                });
            if(customer) {
                customers.push(customer);
            }
        }
        res.status(200).json(customers);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllFavoriteStores(req, res, next) {
    const customerId = req.user.id;
    let stores = [];
    try {
        const favorites = await Favourite.findAll({where: {customerId}});
        const storeIds = favorites.map(record => record.storeId);

        stores = await Store.findAll({
            attributes: {
                exclude: ['baseTransactionId'],
            },where: {id: storeIds}});

        res.status(200).json(stores);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchById(req, res, next) {
    try {
        res.status(200).json(await Customer.findOne({where: {id: req.params.id}}));
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function me(req, res, next) {
    try {
        const user = await Customer.findOne({
            raw: true,
            where: {id: req.user.id},
            attributes: {
                exclude: ['baseTransactionId']
            }
        });
        res.status(200).json(user);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchByName(req, res, next) {
    try {
        const name = req.body.name;
        const condition = {where: {firstName: {[Op.like]: `%${name}%`}}}
        res.status(200).json(await Customer.findAll(condition));
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function remove(req, res, next) {
    try {
        await Customer.destroy({where: {id: req.params.id}});
        res.status(200).json('Completed');
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function addStoreToFavourites(req, res, next) {
    try {
        const {storeId} = req.body;
        const favourite = {
            customerId: req.user.id,
            storeId
        };

        await Favourite.create(favourite);
        res.status(200).json('Completed');
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function removeStoreFromFavourites(req, res, next) {
    try {
        const {storeId} = req.params;
        const customerId = req.user.id;
        await Favourite.destroy({where: {storeId, customerId}});
        res.status(200).json('Completed');
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function checkIfStoreIsFavorite(req, res, next) {
    try {
        const {storeId} = req.params;
        const favorite = await Favourite.findOne({where: {customerId: req.user.id, storeId}});

        res.status(200).json({favorite: favorite ? true : false});
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

async function fetchUserByEmail(req, res, next) {
    try {
        const {email} = req.query;
        const user = await Customer.findOne({
            raw: true,
            where: {identifier: email},
            attributes: {
                exclude: ['baseTransactionId']
            }
        });

        res.status(200).json(user);

    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetchExtraStudentInfo(token) {
    const url = `${process.env.EXTERNAL_API}students/me?$expand=user,person`;
    const config = {
        headers: {Authorization: `Bearer ${token}`}
    };
    return new Promise((resolve, reject) => {
        axios.get(url,
            config
        ).then(data => resolve(data.data)).catch(e => reject(e));
    })
}

async function fetchUserInfoByUniversis(req, res, next) {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const info = await fetchExtraStudentInfo(token);
        const userInfo = {
            firstName: info.person.givenName,
            lastName : info.person.familyName,
            email: info.person.email ? info.person.email : info.user.name,
            phone: info.person.mobilePhone
        };
        res.status(200).json(userInfo);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {
    addStoreToFavourites,
    checkIfStoreIsFavorite,
    fetchAllFavoriteStores,
    fetchAll,
    fetchById,
    fetchByName,
    login,
    me,
    register,
    remove,
    removeStoreFromFavourites,
    fetchAllByStore,
    fetchUserInfoByUniversis,
    fetchUserByEmail,
    externalLogin
}
