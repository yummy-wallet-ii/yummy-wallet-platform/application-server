const db = require("../models");
const {getImageBase64FromFile} = require("../services/file.service");
const {Logging} = require("../services/log.service");
const Category = db.categories;
const BusinessCategory = db.businessCateogies;
const StoreToUser = db.storeToUser;

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        const condition = {where: {userId: +req.user.id}}
        const store = await StoreToUser.findOne(condition);

        const conditionCategories = {where: {storeId: +store.storeId}}
        const categories = await Category.findAll(conditionCategories);
        res.status(200).json(categories);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllBusinessCategories(req, res, next) {
    try {
        res.status(200).json(await BusinessCategory.findAll());
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllCustomer(req, res, next) {
    try {
        const conditionCategories = {where: {storeId: +req.query.storeId}}
        const categories = await Category.findAll(conditionCategories)
        res.status(200).json(categories);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        const category = req.body;
        const file = req.file;
        category.image = `${file.destination}/${file.originalname}`;
        await Category.create(category);
        const log = {
            userId: req.user.id,
            storeId: category.storeId,
            actionId: 1,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Δημιούργησε τη κατηγορία ${category.name} - ${category.description}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function update(req, res, next) {
    try {
        const category = req.body;
        const condition = {where: {id: +category.id}}
        await Category.update(category, condition);

        const log = {
            userId: req.user.id,
            storeId: category.storeId,
            actionId: 2,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Ενημέρωσε τη κατηγορία ${category.name} - ${category.description}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function updateImage(req, res, next) {
    try {
        const {id} = req.body;
        const condition = {where: {id: id}}
        const category = await Category.findByPk(condition);
        const file = req.file;

        const categoryToUpdate = {
            id: category.id,
            name: category.name,
            description: category.description,
            image: `${file.destination}/${file.originalname}`
        }

        await Category.update(categoryToUpdate, condition);

        const log = {
            userId: req.user.id,
            storeId: category.storeId,
            actionId: 2,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Ενημέρωσε τη φωτογραφία της κατηγορίας ${category.name} - ${category.description}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function getCategoryImage(req, res, next) {
    try {
        const condition = {where: {id: +req.params.id}}
        const category = await Category.findOne(condition);
        if (category.image !== '' && category.image !== null) {
            const image = await getImageBase64FromFile(category.image)
            res.status(200).json(image);
        } else {
            res.status(200).json('');
        }
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while fetching category image."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function deleteCategory(req, res, next) {
    try {
        const condition = {where: {id: +req.params.id}}
        const category = await Category.findOne(condition);
        await Category.destroy(condition);

        const log = {
            userId: req.user.id,
            storeId: category.storeId,
            actionId: 3,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Διέγραψε τη κατηγορία ${category.name} - ${category.description}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

module.exports = {
    fetchAll,
    fetchAllBusinessCategories,
    fetchAllCustomer,
    create,
    deleteCategory,
    update,
    updateImage,
    getCategoryImage
}
