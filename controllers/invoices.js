const db = require("../models");
const {Logging} = require("../services/log.service");
const moment = require("moment");
const {fetchStoreById} = require("../services/store.service");
const {
    initializeDocumentStructure,
    addHeader,
    addSpace,
    addValue,
    addSpaceSmall,
    addRowLabel,
    downloadPDF
} = require("../services/pdf.service");
const {fetchInvoiceById} = require("../services/invoice.service");
const fs = require("fs");
const path = require("path");
const Invoice = db.invoices;
const Category = db.categories;

async function fetchById(req, res, next) {
    try {
        const {id} = req.params;
        const invoice = await Invoice.findOne({where: {id}});
        if (!invoice) {
            return res.status(404).json({message: 'Το τιμολόγιο δε βρέθηκε'})
        }

        const store = await fetchStoreById(invoice.store_id)
        const categoryCondition = {where: {id: store.categoryId}}
        const category = await Category.findOne(categoryCondition);
        const model = {
            id: invoice.id,
            store,
            category: category.description,
            price: invoice.price,
            price_tax: invoice.price * 0.024,
            dateCreated: moment(invoice.dateCreated).format('DD-MM-YYYY HH:mm')
        }
        model.final_price = model.price + model.price_tax;

        res.status(200).json(model);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving invoices."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        const allInvoices = [];
        const invoices = await Invoice.findAll();
        for (const invoice of invoices) {
            const store = await fetchStoreById(invoice.store_id)
            const categoryCondition = {where: {id: store.categoryId}}
            const category = await Category.findOne(categoryCondition);
            const model = {
                id: invoice.id,
                store_name: store.legalName,
                category: category.description,
                afm: store.taxNumber,
                address: `${store.street} ${store.postalCode}`,
                phone: store.telephone,
                email: store.email,
                price: invoice.price,
                price_tax: invoice.price * 0.024,
                dateCreated: moment(invoice.dateCreated).format('MM-DD-YYYY HH:mm')
            }
            model.final_price = model.price + model.price_tax;

            allInvoices.push(model);
        }

        res.status(200).json(allInvoices);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving invoices."}
        next();
    }
}

async function create(req, res, next) {
    try {
        const {invoice} = req.body;
        await Invoice.create(invoice);
        return res.status(200).json({message: 'done'});
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while creating invoice."}
        next();
    }
}

async function exportPDF(req, res, next) {
    try {
        const {id} = req.params;
        const invoice = await fetchInvoiceById(id);
        res.status(200).json(await createPDF(invoice));
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while creating invoice pdf."}
        next();
    }
}

async function createPDF(invoice) {
    try {
        // let documentStructure = initializeDocumentStructure();
        //
        // documentStructure = addHeader(documentStructure, `Τιμολόγιο`);
        // documentStructure = addHeader(documentStructure, `Αριθμός τιμολογίου: #${invoice.id}`);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        //
        // documentStructure = addRowLabel(documentStructure, 'Πληροφορίες πελάτη');
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addSpace(documentStructure);
        // documentStructure = addValue(documentStructure, 'Όνομα πελάτη:', invoice?.store.legalName);
        // documentStructure = addSpaceSmall(documentStructure);
        // documentStructure = addValue(documentStructure, 'Διεύθυνση:', invoice?.store.headquarters);
        // documentStructure = addSpaceSmall(documentStructure);
        // documentStructure = addValue(documentStructure, 'Tηλέφωνο:', invoice?.store.telephone);
        // documentStructure = addSpaceSmall(documentStructure);
        // documentStructure = addValue(documentStructure, 'Email:', invoice?.store.email);
        // documentStructure = addSpaceSmall(documentStructure);
        // documentStructure = addValue(documentStructure, 'Α.Φ.Μ:', invoice?.store.taxNumber);
        // documentStructure = addSpaceSmall(documentStructure);

        const documentDefinition = {
            content: [
                {
                    image: `data:image;base64,${
                        fs.readFileSync(path.join(__dirname, '../assets/images/logo.png')).toString('base64')
                    }`,
                    style: 'logo',
                    width: 50
                },
                {
                    text: 'Τιμολόγιο',
                    style: 'header',
                },
                {
                    text: `Αριθμός τιμολογίου: #${invoice.id}`,
                    style: 'subheader',
                },
                {
                    table: {
                        widths: ['*', 'auto'],
                        body: [
                            [{
                                text: `Πληροφορίες πελάτη`,
                                style: 'label',
                            }, {
                                text: `Πληροφορίες τιμολογίου`,
                                style: 'label',
                            },],
                            ['', ''],
                            [{
                                text: `Όνομα πελάτη: ${invoice.store.legalName}`,
                                style: 'info',
                            }, {
                                text: `Ημερομηνία έκδοσης: ${invoice.dateCreated}`,
                                style: 'info',
                            }],
                            [{
                                text: `Διεύθυνση: ${invoice.store.headquarters}`,
                                style: 'info',
                            }, ''],
                            [{
                                text: `Τηλέφωνο: ${invoice.store.telephone}`,
                                style: 'info',
                            }, ''],

                            [{
                                text: `Email: ${invoice.store.email}`,
                                style: 'info',
                            }, ''],

                            [{
                                text: `Α.Φ.Μ: ${invoice.store.taxNumber}`,
                                style: 'info',
                            }, ''],
                        ],
                    },
                    layout: 'noBorders',
                },
                {
                    text: ``,
                    style: 'emptyLine',
                },
                {
                    table: {
                        widths: ['*', 'auto', 'auto', 'auto'],
                        body: [
                            ['Περιγραφή', 'Ποσότητα', 'Τιμή', 'Σύνολο'],
                            [{
                                text: `Υπηρεσίες Yummy Wallet`,
                                style: 'info',
                            },
                                {
                                    text: `1`,
                                    style: 'info',
                                },
                                {
                                    text: `${invoice?.price} €`,
                                    style: 'info',
                                },
                                {
                                    text: `${invoice?.final_price} €`,
                                    style: 'info',
                                },
                            ],
                        ],
                        layout: 'table',
                    },
                },
                {
                    text: ``,
                    style: 'emptyLine',
                },
                {
                    table: {
                        widths: ['*', 'auto'],
                        body: [
                            ['', `Σύνολο: ${invoice?.final_price} €`],
                        ],
                    },
                    layout: 'noBorders',
                },
            ],
            defaultStyle: {
                font: 'CenturyGothic',
            },
            styles: {
                logo: {
                    alignment: 'center',
                    margin: [0, -10, 0, 10]
                },
                table: {
                    border: 0.2
                },
                header: {
                    fontSize: 18,
                    alignment: 'center',
                    margin: [0, 0, 0, 10],
                },
                subheader: {
                    fontSize: 12,
                    alignment: 'center',
                    margin: [0, 0, 0, 20],
                },
                total: {
                    fontSize: 14,
                    margin: [0, 10, 0, 10],
                },
                emptyLine: {
                    margin: [0, 0, 0, 10]
                },
                label: {
                    fontSize: 12,
                    bold: true
                },
                info: {
                    fontSize: 12,
                }
            },
        };

        return await downloadPDF(documentDefinition);
    } catch (e) {
        throw e;
    }
}

module.exports = {
    fetchAll,
    fetchById,
    create,
    exportPDF
}
