const db = require("../models");
const {getImageBase64FromFile} = require("../services/file.service");
const {Logging} = require("../services/log.service");
const {fetchCustomerStamps} = require("../services/stamp.service");
const Product = db.products;
const CustomerStamps = db.customerStamps;

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        const id = +req.query.storeId;
        const condition = {where: {storeId: id}}

        res.status(200).json(await Product.findAll(condition));
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving products."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllCustomer(req, res, next) {
    try {
        const id = +req.query.storeId;
        const categoryId = +req.query.categoryId;
        const condition = {where: {storeId: id, categoryId}}
        const products = await Product.findAll(condition)
        res.status(200).json(products);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving products."}
        next();
    }
}

async function fetchAllCustomerStamps(req, res, next) {
    try {
        const customerId = req.user.id;
        const categoryId = +req.query.categoryId;

        const stamps = await fetchCustomerStamps(categoryId, customerId);
        res.status(200).json(stamps);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving products."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchById(req, res, next) {
    try {
        const id = +req.params.id;
        const condition = {where: {id: id}}

        res.status(200).json(await Product.findOne(condition));
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while retrieving products."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        const product = req.body;
        product.image = req.file.path;
        product.storeId = req.query.storeId;
        await Product.create(product);

        const log = {
            userId: req.user.id,
            storeId: +req.query.storeId,
            actionId: 1,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Δημιούργησε το προϊόν ${product.title}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving product."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchImage(req, res, next) {
    try {
        const condition = {where: {id: +req.params.id}}
        const product = await Product.findOne(condition);
        if (product.image !== '' && product.image !== null) {
            const image = await getImageBase64FromFile(product.image)
            res.status(200).json(image);
        } else {
            res.status(200).json('');
        }
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while fetching images."}
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function update(req, res, next) {
    try {
        const product = req.body;
        const condition = {where: {id: +req.body.id}}
        await Product.update(product, condition);

        const log = {
            userId: req.user.id,
            storeId: +req.query.storeId,
            actionId: 2,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Ενημέρωσε το προϊόν ${product.title}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

/**
 * Description update product's image
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function updateImage(req, res, next) {
    try {
        const condition = {where: {id: +req.body.id}}
        const product = Product.findOne(condition);
        product.image = req.file.path;
        await Product.update(product, condition);

        const log = {
            userId: req.user.id,
            storeId: +req.query.storeId,
            actionId: 2,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Ενημέρωσε τη φωτογραφία του προϊόντος ${product.title}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

/**
 * Description delete product
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function deleteProduct(req, res, next) {
    try {
        const condition = {where: {id: +req.params.id}}
        const product = Product.findOne(condition);

        await Product.destroy(condition);

        const log = {
            userId: req.user.id,
            storeId: +req.query.storeId,
            actionId: 3,
            isAdmin: false,
            isConsumer: false,
            typeId: 4,
            message: `Διέγραψε το προϊόν ${product.title}`
        };

        await Logging(log);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while saving categories."}
        next();
    }
}

module.exports = {
    fetchAll,
    create,
    update,
    fetchById,
    fetchAllCustomerStamps,
    deleteProduct,
    fetchImage,
    updateImage,
    fetchAllCustomer
}
