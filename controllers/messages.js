const {sendNotifications} = require("../firebase/push-notifications");
const {sendEmailToRecipients} = require("../services/email");
const {fetchCustomerByToken, fetchCustomerByIdentifier} = require("../services/customerService");
const {logSentNotification, logSentEmail} = require("../services/log.service");

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function create(req,res,next) {
    try {
        const {recipients, storeId, notification} = req.body;
        await sendNotifications(recipients, notification.title, notification.body);
        for (const recipient of recipients) {
            const customer = await fetchCustomerByToken(recipient);
            if(customer) {
                const log = {
                    storeId,
                    customerId: customer.id,
                    title: notification.title,
                    sendDate: new Date(),
                };
                await logSentNotification(log);
            }
        }
        return res.status(200).json('ok');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function sendEmail(req,res,next) {
    try {
        const {recipients, storeId, notification} = req.body;
        for (const recipient of recipients) {
            await sendEmailToRecipients(notification.body, notification.title, recipient , null, 'yummyWallet@ysoft.gr');
            const customer = await fetchCustomerByIdentifier(recipient);
            if(customer) {
                const log = {
                    storeId,
                    customerId: customer.id,
                    title: notification.title,
                    sendDate: new Date(),
                };
                await logSentEmail(log);
            }
        }
        return res.status(200).json('ok');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {create, sendEmail}
