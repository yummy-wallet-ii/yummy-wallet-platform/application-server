const db = require("../models");
const User = db.users;
const {validatePassword, signJsonWebToken, generateHashedPassword} = require("../utilities/authentication");
const axios = require('axios');

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function adminLogin(req, res, next) {
    try {
        const user = req.body;
        const condition = {where: {identifier: user.identifier, roleId: 1}};
        const users = await User.findAll(condition);
        const currentUser = users[0];

        const match = await validatePassword(user.password, currentUser.password);
        if (match) {
            const token = signJsonWebToken(currentUser.id);
            res.status(200).json(token);
        } else {
            res.status(404).json('User not found');
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function login(req, res, next) {
    try {
        const user = req.body;
        const condition = {where: {identifier: user.identifier, provider: 'local'}};
        const users = await User.findAll(condition);
        if(users.length > 0) {
            const currentUser = users[0];

            const match = await validatePassword(user.password, currentUser.password);
            if (match) {
                const token = signJsonWebToken(currentUser.id);
                res.status(200).json(token);
            } else {
                res.status(404).json('User not found');
            }
        } else {
            res.status(404).json('User not found');
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function externalLogin(req, res, next) {
    try {
        const user = req.body;
        const condition = {where: {identifier: user.identifier, provider: user.provider}};
        const users = await User.findAll(condition);
        if(users.length > 0) {
            const currentUser = users[0];
            if (currentUser) {
                const token = signJsonWebToken(currentUser.id);
                res.status(200).json({token, message: null});
            } else {
                res.status(404).json({token: null, message: 'User not found'});
            }
        } else {
            res.status(404).json({token: null, message: 'User not found'});
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function register(req, res, next) {
    try {
        const user = req.body;
        const hashedPassword = await generateHashedPassword(user.password);
        const newUser = {
            identifier: user.identifier,
            password: hashedPassword,
            provider: 'local'
        }

        await User.create(newUser);
        res.status(200).json('Completed');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        const users = await User.findAll();
        for (const user of users) {
            delete user.password;
        }
        res.status(200).json(users);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchUserById(req, res, next) {
    try {
        const {id} = req.params;
        const condition = {where: {id}};
        const user = await User.findOne(condition);
        delete user.password;

        res.status(200).json(user);

    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 * Description FetchRequested User
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchRequestedUser(req, res, next) {
    try {
        res.status(200).json(req.user);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function updateUser(req, res) {
    res.status(200).json("OK");
}

/**
 * Description Deletes User
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function deleteUser(req, res, next) {
    try {
        const condition = {where: {id: req.params.id}}

        await User.destroy(condition);

        res.status(200).json('Completed');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}


module.exports = {
    deleteUser,
    fetchAll,
    fetchUserById,
    fetchRequestedUser,
    login,
    register,
    updateUser,
    adminLogin,
    externalLogin,
}
