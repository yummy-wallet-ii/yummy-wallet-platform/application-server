/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function assign(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function redeem(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchByCustomerAssigned(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 * Remove a coupon after it's used
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function remove(req, res, next) {
    try {
        res.status(200).json();
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

module.exports = {
    create,
    assign,
    redeem,
    fetchAll,
    fetchByCustomerAssigned,
    remove
}
