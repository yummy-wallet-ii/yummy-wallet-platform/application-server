const db = require("../models");
const Transaction = db.transaction;
const Customer = db.customer;
const sequelize = require("sequelize");
const {QueryTypes} = require("@sequelize/core");
const {createTransaction} = require("../services/transaction.service");
const {Stripe} = require("stripe");
const {fetchStoreById} = require("../services/store.service");
const {sendNotifications} = require("../firebase/push-notifications");
const {fetchCustomerById} = require("../services/customerService");

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchByStoreId(req, res, next) {
    try {
        const {storeId} = req.params;
        const transactionsRequest = await Transaction.findAll({
            raw: false,
            where: {storeId},
            attributes: [
                "id",
                "userId",
                "storeId",
                "customerId",
                "createdAt",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "dateCreated"]
            ],
            include: [{
                model: Customer,
                as: 'customer',
                attributes: {
                    exclude: ["baseTransactionId"],
                    include: ["firstName", "lastName"]
                }
            }],
            subQuery: true,
            order: [['createdAt', 'desc']]
        });
        res.status(200).json(transactionsRequest);
    } catch (e) {
        console.log(e);
        req.error = {message: e.message || 'Error while fetching transactions'};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function stripeInitPayment(req, res, next) {
    try {
        const {merchantId, amount} = req.body;
        const store = await fetchStoreById(merchantId);

        if (!store) {
            return res.status(404).json('store not found');
        }

        const stripe = new Stripe(store.secretKey, {apiVersion: '2022-08-01', typescript: true});

        const paymentIntent = await stripe.paymentIntents.create({
            amount: +amount * 100,
            currency: 'eur',
            payment_method: 'pm_card_visa',
            receipt_email: 'g.rouzios@ysoft.gr'
        });

        res.status(200).json({
            clientSecret: paymentIntent.client_secret,
        })
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function stripePayment(req, res, next) {
    try {

        const {merchantId, amount, email} = req.body;
        const store = await fetchStoreById(merchantId);

        if (!store) {
            return res.status(404).json('store not found');
        }

        const stripe = new Stripe(store.secretKey, {apiVersion: '2022-08-01', typescript: true});

        const customers = await stripe.customers.list();
        let customer = customers.data.find(customer => customer.email === email);

        if (!customer) {
            customer = await stripe.customers.create({
                email
            });
        }


        if (!customer) {
            res.status(500).json('You have no customer created')
        }

        const ephemeralKey = await stripe.ephemeralKeys.create({
                customer: customer.id
            },
            {
                apiVersion: '2022-08-01'
            });

        const paymentIntent = await stripe.paymentIntents.create({
            amount: +amount * 100,
            currency: 'eur',
            customer: customer.id,
            payment_method_types: ['card'],
        });

        res.status(200).json({
            paymentIntent: paymentIntent.client_secret,
            ephemeralKey: ephemeralKey.secret,
            customer: customer.id,
            clientSecret: paymentIntent.client_secret,
        })
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchByCustomerId(req, res, next) {
    try {
        const customerId = req.user.id;
        const transactionsQuery = `select * from transactions_data where customerId = ${customerId} order by id desc`;
        const transactions = await db.sequelize.query(transactionsQuery, {
            logging: console.log,
            type: QueryTypes.SELECT,
            plain: false,
            raw: true
        });

        res.status(200).json(transactions);

    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

/**
 * Description: Create a new transaction
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        const transaction = {
            storeId: req.body.storeId,
            customerId: req.body.customerId,
            price: req.body.price,
        }
        await createTransaction(transaction, req.body.order);
        const customer = await fetchCustomerById(req.body.customerId);
        await sendNotifications([customer.fcmToken], 'Επιτυχής συναλλαγή', 'Η συναλλαγή σας ολοκληρώθημε με επιτυχία');
        res.status(200).json('ok');
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

module.exports = {
    create,
    fetchByStoreId,
    fetchByCustomerId,
    stripePayment,
    stripeInitPayment
}
