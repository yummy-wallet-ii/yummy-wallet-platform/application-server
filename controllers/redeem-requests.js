const db = require("../models");
const moment = require("moment");
const {fetchByCreditsCustomerId, redeemCredits} = require("../services/credits.service");
const {postToBlockChain, BlockChainEndpoints} = require("../services/blockchain.service");
const RedeemRequests = db.redeemRequests;
const Customer = db.customer;

async function fetchByCustomerId(req, res, next) {
    try {
        const {id} = req.user;
        const options = {
            include: [
                {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        };
        const requests = [];
        const data = await RedeemRequests.findAll({where: {customerId: id}, include: options.include, subQuery: options.subQuery});
        for (const record of data) {
            const model = {
                id: record.id,
                customerFirstName: record.base_customer.firstName,
                customerLastName: record.base_customer.lastName,
                customerEmail: record.base_customer.identifier,
                customerMobile: record.base_customer.mobile,
                customerIBAN: record.base_customer.IBAN,
                amount: record.amount,
                status: record.status,
                requestDate: moment(record.requestDate).format("DD-MM-YYYY HH:mm"),
                actionDate: record.actionDate ? moment(record.actionDate).format("DD-MM-YYYY HH:mm") : null
            }
            requests.push(model);
        }

        return res.status(200).json(requests);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetch(req, res, next) {
    try {
        const options = {
            include: [
               {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        };
        const requests = [];
        const data = await RedeemRequests.findAll(options);
        for (const record of data) {
            const model = {
                id: record.id,
                customerFirstName: record.base_customer.firstName,
                customerLastName: record.base_customer.lastName,
                customerEmail: record.base_customer.identifier,
                customerMobile: record.base_customer.mobile,
                customerIBAN: record.base_customer.IBAN,
                amount: record.amount,
                status: record.status,
                requestDate: moment(record.requestDate).format("MM-DD-YYYY HH:mm"),
                actionDate: record.actionDate ? moment(record.actionDate).format("MM-DD-YYYY HH:mm") : null
            }
            requests.push(model);
        }

        return res.status(200).json(requests);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function create(req, res, next) {
    try {
        const request = req.body;
        request.customerId = req.user.id;
        const currentCredits = await fetchByCreditsCustomerId(req.user.id);
        if(currentCredits.credits - request.amount < 0) {
           return res.status(400).json({message: 'Το υπόλοιπό σας δεν επαρκεί'});
        } else {
            await RedeemRequests.create(request);
            return res.status(200).json({message: 'Το αίτημα καταχωρήθηκε'});
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function updateRequest(req, res, next) {
    try {
        const {id} = req.params;
        const {request} = req.body;
        const redeemRequest = await RedeemRequests.findOne({where: {id}});
        let blockChainTransactionId = null;
        if(request.status === 'COMPLETED') {
            if(!redeemRequest) { return res.status(404).json({message: 'Δε βρέθηκε το αίτημα με αυτό το αναγνωριστικό'})}
            await redeemCredits(redeemRequest.customerId, redeemRequest.amount);
        }

        const customerCredits = await fetchByCreditsCustomerId(redeemRequest.customerId);


        const blockChainBody = {
            customerId: redeemRequest.customerId,
            ywId: 'YSOFT',
            price: +redeemRequest.amount,
            coinsToRedeem: redeemRequest.amount,
            coins: customerCredits.credits,
            status: request.status,
            transactionDate: new Date().getTime(),
            actionId: 3
        }

        const {status, message, data } = await postToBlockChain(BlockChainEndpoints.YSOFTPayment, blockChainBody);
        if (status === 200) {
            blockChainTransactionId = data.id;
        }

        await RedeemRequests.update(
            {
                status: request.status,
                blockChainTransactionId,
                actionDate: new Date()
            },
            {
                where: {id}
            }
        );

        return res.status(200).json({message: 'Το αίτημα ενημερώθηκε'});
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {fetch, create, updateRequest, fetchByCustomerId}
