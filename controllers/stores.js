const db = require("../models");
const {generateHashedPassword} = require("../utilities/authentication");
const Store = db.stores;
const User = db.users;
const StoreOptions = db.storeOptions;
const StoreToUser = db.storeToUser;
const {QueryTypes} = require('@sequelize/core');
const path = require("path");
const fs = require("fs");
const {base64Image_encode} = require("../utilities/file");
const {fetchAllStores, fetchStoreById, fetchStoreOptionsByStoreId, fetchStoreRefills, completeRefill,
    fetchAllUnregisteredStores, updateStoreBucket, fetchRegisteredStores
} = require("../services/store.service");
const moment = require("moment");
const {postToBlockChain, BlockChainEndpoints} = require("../services/blockchain.service");

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function create(req, res, next) {
    try {
        const store = req.body;
        let userId = null;
        const user = {
            identifier: store.email,
            password: await generateHashedPassword(store.password),
            provider: store.provider,
            roleId: 2
        }

        const createUserRequest = await User.create(user);
        userId = createUserRequest?.dataValues?.id;

        if (!userId) {
            return res.status(400).json(`Couldn't create user`);
        }

        delete store.password;
        delete store.provider;
        store.schedule = JSON.stringify(store.schedule);
        const createStoreRequest = await Store.create(store);
        const storeId = createStoreRequest?.dataValues?.id;

        if (!storeId) {
            const condition = {where: {id: userId}}
            await User.destroy(condition);
            return res.status(400).json(`Couldn't create store`);
        }

        await StoreOptions.create({storeId});

        await StoreToUser.create({userId, storeId});

        res.status(200).json(`ok`);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        res.status(200).json(await fetchAllStores(1));
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetchRegistered(req, res, next) {
    try {
        res.status(200).json(await fetchRegisteredStores());
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetchRefills(req, res, next) {
    try {
        const {storeId} = req.params;
        res.status(200).json(await fetchStoreRefills(+storeId));
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function createStoreRefill(req, res, next) {
    try {
        const {storeId} = req.params;

        const blockChainBody = {
            storeId,
            ywId: "YSOFT",
            price: +req.body.amount,
            transactionDate: new Date().getTime(),
            actionId: 4
        }
        const {status, message, data } = await postToBlockChain(BlockChainEndpoints.StorePayment, blockChainBody);
        if (status === 200) {
            const blockChainTransactionId = data.id;
            const refill = {
                storeId,
                amount: +req.body.amount,
                transactionIntentId: req.body.transactionIntentId,
                transactionDate: new Date(),
                blockChainTransactionId
            }
            await completeRefill(refill);
            await updateStoreBucket(storeId, +req.body.amount);
            res.status(200).json({message: 'Refill Completed'});
        } else {
            req.error = { message };
            next();
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllRequested(req, res, next) {
    try {
        res.status(200).json(await fetchAllUnregisteredStores(0));
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function fetchById(req, res, next) {
    try {
        const {id} = req.params;
        const store = await fetchStoreById(id);

        return store ?  res.status(200).json(store) : res.status(404).json("Store not found");
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function remove(req, res, next) {
    try {
        const condition = {where: {id: req.params.id}}

        await Store.destroy(condition);

        res.status(200).json('Completed');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function update(req, res, next) {
    try {
        const {id} = req.params;
        const {storeInfo, storeOptions} = req.body;

        await Store.update(storeInfo, {where: {id}});
        await StoreOptions.update(storeOptions, {where: {storeId: id}});

        res.status(200).json('done');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchUserStores(req, res, next) {
    try {
        const stores = await db.sequelize.query(`select * from user_stores_data where userId = ${req.user.id}`, {
            logging: console.log,
            type: QueryTypes.SELECT,
            plain: false,
            raw: true
        });

        res.status(200).json(stores);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchStoreOptions(req, res, next) {
    try {
        const {storeId} = req.params;
        const options = await fetchStoreOptionsByStoreId(storeId);
        res.status(200).json(options);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function insertOrUpdateStoreFrontImage(req, res, next) {
    try {
        const file = req.file;
        if (!file) {
            next({
                message: "No File Found"
            });
        }
        const storeId = +req.query.storeId;
        await StoreOptions.update(
            {frontImage: `./uploads/${storeId}/${file.originalname}`},
            {where: {storeId}}
        );
        res.status(200).json('Update successful');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function fetchStoreFrontImage(req, res, next) {
    try {
        const {storeId} = req.params;
        const condition = {where: {storeId}};
        const storeOptionsRequest = await StoreOptions.findOne(condition);
        const storeOptions = storeOptionsRequest.dataValues;
        const imagePath = storeOptions.frontImage;
        if (imagePath) {
            const finalPath = path.join(__dirname, `../${imagePath}`);
            return res.sendFile(finalPath);
        } else {
            res.status(404).json(null);
        }
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchStoreFrontImageBase64(req, res, next) {
    try {
        const {storeId} = req.params;
        const condition = {where: {storeId}};
        const storeOptionsRequest = await StoreOptions.findOne(condition);
        const storeOptions = storeOptionsRequest.dataValues;
        const imagePath = storeOptions.frontImage;
        if (imagePath) {
            const finalPath = path.join(__dirname, `../${imagePath}`);

            const base64str = await base64Image_encode(finalPath);
            res.status(200).json(base64str);
        } else {
            res.status(404).json(null);
        }

    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function removeStoreFrontImage(req, res, next) {
    try {
        const {storeId} = req.params;
        const condition = {where: {storeId}};
        const storeOptionsRequest = await StoreOptions.findOne(condition);
        const storeOptions = storeOptionsRequest.dataValues;
        const imagePath = storeOptions.frontImage;

        fs.unlinkSync(imagePath);
        await StoreOptions.update(
            {frontImage: null},
            {where: {storeId}}
        );
        res.status(200).json('Update successful');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}
async function activationStatusUpdate(req, res, next) {
    try {
        const {storeId, status} = req.body;
        const info = {
            isActive: +status,
        }

        await Store.update(info, {where: {id: storeId}});

        res.status(200).json({message: 'Store action completed successfully'});
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}
async function registerStore(req, res, next) {
    try {
        const {storeId} = req.body;
        const info = {
            isRegistered: 1,
            registrationDate: new Date(),
        }

        await Store.update(info, {where: {id: storeId}});

        res.status(200).json({message: 'Store registered successfully'});
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}
/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function updateGeoFence(req, res, next) {
    try {
        const {storeId} = req.params;
        const {title, body, isActive} = req.body;
        const info = {
            geoFenceTitle: title,
            geoFenceBody: body,
            geoFenceActive: isActive
        }

        await StoreOptions.update(info, {where: {storeId}});
        res.status(200).json('Update successful');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchStoreLogs(req, res, next) {
    try {
        const {storeId} = req.params;
        const logs = await db.sequelize.query(`select * from stores_logs_data where storeId = ${storeId}`, {
            logging: console.log,
            type: QueryTypes.SELECT,
            plain: false,
            raw: true
        });
        res.status(200).json(logs);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {
    create,
    fetchAll,
    registerStore,
    fetchById,
    activationStatusUpdate,
    fetchRefills,
    createStoreRefill,
    remove,
    fetchUserStores,
    update,
    insertOrUpdateStoreFrontImage,
    fetchStoreFrontImage,
    removeStoreFrontImage,
    fetchStoreOptions,
    fetchStoreFrontImageBase64,
    updateGeoFence,
    fetchStoreLogs,
    fetchAllRequested,
    fetchRegistered
}
