const shortid = require('shortid');
const db = require("../models");
const sequelize = require("sequelize");
const {sendSocketNotification} = require("../services/socket-notifications.service");
const {updateCustomerStamps} = require("../services/stamp.service");
const {fetchCategoryById} = require("../routes/category.service");
const Order = db.orders;
const OrderDetails = db.ordersDetails;
const StoreToUser = db.storeToUser;
const Stores = db.stores;
const Customer = db.customer;

//#region Merchant
/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllPending(req, res, next) {
    try {
        const condition = {where: {userId: +req.user.id}}
        const store = await StoreToUser.findOne(condition);

        const orders = await Order.findAll({
            where: {storeId: store.storeId, status: 0},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "orderDate"]
            ],
            include: [
                {
                    required: true,
                    model: OrderDetails,

                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        })

        res.status(200).json(orders);

    } catch (e) {
        req.error = {message: e.message || 'Error while fetching pending orders'};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function fetchById(req, res, next) {
    try {
        const condition = {where: {userId: +req.user.id}}
        const store = await StoreToUser.findOne(condition);

        const order = await Order.findOne({
            where: {storeId: store.storeId, id: +req.params.id},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                "status",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "orderDate"]
            ],
            include: [
                {
                    required: true,
                    model: OrderDetails,

                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        })

        return res.status(200).json(order);
    } catch (e) {
        req.error = {message: e.message || `Error while fetching order with id ${req.params.id}`};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAllCompleted(req, res, next) {
    try {
        const condition = {where: {userId: +req.user.id}}
        const store = await StoreToUser.findOne(condition);

        const orders = await Order.findAll({
            where: {storeId: store.storeId, status: 1},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "orderDate"],
                [sequelize.fn("DATE_FORMAT", sequelize.col("completedAt"), "%d-%m-%Y %H:%i:%s"), "completedAt"]
            ],
            include: [
                {
                    required: true,
                    model: OrderDetails,

                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        })

        res.status(200).json(orders);

    } catch (e) {
        req.error = {message: e.message || 'Error while fetching pending orders'};
        next();
    }
}

async function fetchAllCompletedByStoreId(req, res, next) {
    try {
        const {storeId} = req.params;
        const orders = await Order.findAll({
            where: {storeId, status: 1},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "orderDate"],
                [sequelize.fn("DATE_FORMAT", sequelize.col("completedAt"), "%d-%m-%Y %H:%i:%s"), "completedAt"]
            ],
            include: [
                {
                    required: true,
                    model: OrderDetails,

                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        })

        res.status(200).json(orders);

    } catch (e) {
        req.error = {message: e.message || 'Error while fetching pending orders'};
        next();
    }
}

async function fetchAll(req, res, next) {
    try {
        const orders = await Order.findAll({
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i:%s"), "orderDate"],
                [sequelize.fn("DATE_FORMAT", sequelize.col("completedAt"), "%d-%m-%Y %H:%i:%s"), "completedAt"]
            ],
            include: [
                {
                    required: true,
                    model: OrderDetails,

                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                },
                {
                    model: Stores,
                    source: 'storeId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["legalName", "taxNumber"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }
            ],
            subQuery: true,
        })

        res.status(200).json(orders);

    } catch (e) {
        req.error = {message: e.message || 'Error while fetching pending orders'};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function complete(req, res, next) {
    try {
        await Order.update(
            {
                status: 1,
                completedAt: new Date()
            },
            {
                where: {id: +req.params.id}
            }
        );
        return res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || 'Error while completing order'};
        next();
    }
}

//#endregion
//#region Customer - Consumer
/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        const socket = req.app.get('socket');
        const {storeId, items, price} = req.body;
        const order = {
            orderUniqueId: shortid.generate(),
            storeId,
            customerId: req.user.id,
            status: 0,
            createdAt: new Date(),
            price
        }
        const request = await Order.create(order);
        const orderId = request.id;
        for (const item of items) {
            const details = {
                orderId,
                productId: item.id,
                productPrice: item.price,
                storeId,
                comments: item.comments,
            };

            const category = await fetchCategoryById(item.categoryId);
            if (category && category.hasStamps) {
                await updateCustomerStamps(item.categoryId, req.user.id, 1, category);
            }
            await OrderDetails.create(details);
        }
        await sendSocketNotification('orders', storeId, JSON.stringify(req.body), socket);
        res.status(200).json('done');
    } catch (e) {
        req.error = {message: e.message || 'Error while creating order'};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchCustomerPending(req, res, next) {
    try {
        const customerId = req.user.id;
        const orders = await Order.findAll({
            raw: true,
            where: {customerId: customerId, status: 0},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i"), "orderDate"]
            ],
        })

        for (const order of orders) {
            const store = await Stores.findOne({
                where: {id: order.storeId},
                attributes: {
                    exclude: ["baseTransactionId"]
                }
            });
            order.storeName = store.legalName;
        }

        res.status(200).json(orders);
    } catch (e) {
        req.error = {message: e.message || 'Error while fetching order'};
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchCustomerCompleted(req, res, next) {
    try {
        const customerId = req.user.id;
        const orders = await Order.findAll({
            raw: true,
            where: {customerId: customerId, status: 1},
            attributes: [
                "id",
                "orderUniqueId",
                "storeId",
                "customerId",
                "price",
                [sequelize.fn("DATE_FORMAT", sequelize.col("createdAt"), "%d-%m-%Y %H:%i"), "orderDate"]
            ]
        })
        for (const order of orders) {
            const store = await Stores.findOne({
                where: {id: order.storeId},
                attributes: {
                    exclude: ["baseTransactionId"]
                }
            });
            order.storeName = store.legalName;
        }
        res.status(200).json(orders);
    } catch (e) {
        req.error = {message: e.message || 'Error while fetching order'};
        next();
    }
}

//#endregion

module.exports = {
    complete,
    create,
    fetchAllCompleted,
    fetchAllCompletedByStoreId,
    fetchAllPending,
    fetchById,
    fetchAll,
    fetchCustomerCompleted,
    fetchCustomerPending,
}
