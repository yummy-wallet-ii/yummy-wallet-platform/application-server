const {postToBlockChain, fetchFromBlockChain} = require("../services/blockchain.service");
const {fetchCustomerByToken} = require("../services/customerService");

async function fetch(req, res,next) {
    try {
       const data =  await fetchFromBlockChain('customer-transaction/xHx5Fhlaq8ohKtdn83Fu');
       res.status(200).json(data);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}
async function create(req, res,next) {
    try {
        const {body} = req;
      const customer = await fetchCustomerByToken(body.token);
        res.status(200).json(customer);
    } catch (e) {
        req.error = {message: e.message};
        next();
    }
}

module.exports = {
    fetch,
    create
}
