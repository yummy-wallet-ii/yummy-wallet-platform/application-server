const db = require("../models");
const City = db.cities;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchAll(req, res, next) {
    try {
        res.status(200).json(await City.findAll());
    } catch (e) {
        req.error = { message:e.message };
        next();    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchById(req, res, next) {
    try {
        res.status(200).json(await City.findOne({where: {id: req.params.id}}));
    } catch (e) {
        req.error = { message:e.message };
        next();    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function fetchByName(req, res, next) {
    try {
        const name = req.body.name;
        const condition = {where: {name: {[Op.like]: `%${name}%`}}}
        res.status(200).json(await City.findAll(condition));
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function create(req, res, next) {
    try {
        const city = req.body;
        await City.create(city);
        res.status(200).json('Completed');
    } catch (e) {
        req.error = { message:e.message };
        next();    }
}

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
async function remove(req, res, next) {
    try {
        await City.destroy({where: {id: req.params.id}});
        res.status(200).json('Completed');
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {
    fetchAll,
    fetchById,
    fetchByName,
    create,
    remove
}
