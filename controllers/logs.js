const db = require("../models");
const moment = require("moment");
const EmailLogs = db.emailLogs;
const Store = db.stores;
const Customer = db.customer;
const NotificationLogs = db.notificationLogs;
async function fetchNotifications(req, res, next) {
    try {
        const storeId = req.query.storeId;
        const options = {
            include: [
                {
                    model: Store,
                    source: 'storeId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["legalName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        };

        const data = storeId ? await NotificationLogs.findAll({where: {storeId}, include: options.include, subQuery: options.subQuery}) : await NotificationLogs.findAll(options);
        const notifications = [];
        for (const record of data) {
            const model = {
                id: record.id,
                customerId: record.base_customer.id,
                customerName: `${record.base_customer.firstName} ${record.base_customer.lastName}`,
                store: record.base_store.legalName,
                title: record.title,
                sendDate: moment(record.sendDate).format('MM-DD-YYYY HH:mm')
            }
            notifications.push(model);
        }
        return res.status(200).json(notifications);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetchEmails(req, res, next) {
    try {
        const storeId = req.query.storeId;
        const options = {
            include: [
                {
                    model: Store,
                    source: 'storeId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["legalName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }, {
                    model: Customer,
                    source: 'customerId',
                    foreignKey: 'id',
                    required: true,
                    attributes: {
                        include: ["firstName", "lastName"],
                        exclude: ["baseTransactionId", "createdAt"]
                    }
                }],
            subQuery: true,
        };

        const data = storeId ? await EmailLogs.findAll({where: {storeId}, include: options.include, subQuery: options.subQuery}) :
            await EmailLogs.findAll(options);
        const emails = [];
        for (const record of data) {
            const model = {
                id: record.id,
                customerId: record.base_customer.id,
                customerName: `${record.base_customer.firstName} ${record.base_customer.lastName}`,
                store: record.base_store.legalName,
                title: record.title,
                sendDate: moment(record.sendDate).format('MM-DD-YYYY HH:mm')
            }
            emails.push(model);
        }
        return res.status(200).json(emails);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {fetchEmails, fetchNotifications}
