const stripe = require('stripe')('pk_test_51M2vwlD7jeb0MaCLA7alGnVFjQ34QISGsJw8pYCqj531TXronI5CQRtqUtcxapTCkqhjWTVoWXXy1W136i6EUOR800lOd2ppdK');
const db = require("../models");
const {fetchStoreById} = require("../services/store.service");
const moment = require("moment");
const Store = db.stores;
const StoreRefills = db.storeRefill;

async function payment(req, res,next) {
    try {
        const session = await stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            line_items: [
                {
                    price_data: {
                        currency: 'usd',
                        product_data: {
                            name: 'T-shirt',
                        },
                        unit_amount: 2000,
                    },
                    quantity: 1,
                },
            ],
            mode: 'payment',
            success_url: 'https://example.com/success',
            cancel_url: 'https://example.com/cancel',
        });

        res.json({ id: session.id });
    } catch (e) {
        res.status(400).json(e.message);
    }
}

async function fetch(req, res,next) {
    try {
        const payments = [];
        const refills = await StoreRefills.findAll();
        for (const refill of refills) {
            const store = await fetchStoreById(refill.storeId);
            const model = {
                id: refill.id,
                store: store.legalName,
                storeId: store.id,
                email: store.email,
                amount: refill.amount,
                stripeTransactionId: refill.transactionIntentId,
                blockChainTransactionId: refill.blockChainTransactionId,
                transactionDate: moment(refill.transactionDate).format('MM-DD-YYYY HH:mm')
            }
            payments.push(model);
        }

        res.status(200).json(payments);
    } catch (e) {
        req.error = {message: e.message || "Some error occurred while fetching payments."}
        next();
    }
}

module.exports = {payment, fetch}
