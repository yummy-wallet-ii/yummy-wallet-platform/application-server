const {fetchTotalCreditsByUserId, fetchByCreditsCustomerId} = require("../services/credits.service");

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
async function fetchCreditsByUserId(req, res,next) {
    try {
        const credits = await fetchTotalCreditsByUserId(req.user.id)
        return res.status(200).json(credits);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

async function fetch(req, res, next) {
    try {
        const credits = await fetchByCreditsCustomerId(req.user.id);
        return res.status(200).json(credits);
    } catch (e) {
        req.error = { message:e.message };
        next();
    }
}

module.exports = {
    fetchCreditsByUserId,
    fetch
}
