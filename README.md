# application-server

To run the application server there must be included a .env file containing the following

DB_USER='DatabaseUse'
DB_PASSWORD = 'DatabaseUserPassword'
DB_HOST='DatabaseHost'
SECRET='ApplicationSercret'
CUSTOMER_SECRET='CustomerSecret'
TOKEN_EXPIRATION='1d'
NODE_ENV='NODE_ENV'
DATABASE='DatabaseName'
BLOCKCHAIN_API='BlockChainAPIUrl'
CLIENT_ID='BlockChainAPIClientID'
CLIENT_SECRET='BlockChaingAPISecretID'
