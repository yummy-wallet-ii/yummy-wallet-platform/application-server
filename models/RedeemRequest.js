/**
 * Description: Define Redeem Request Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_redeem_request", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        customerId: {type: Sequelize.INTEGER},
        amount: {type: Sequelize.INTEGER},
        status: {type: Sequelize.STRING},
        requestDate: {type: Sequelize.DATE},
        actionDate: {type: Sequelize.DATE},
        blockChainTransactionId: {type: Sequelize.STRING},
    });
};
