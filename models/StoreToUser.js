/**
 * Description: Define Store To User Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_store_to_user", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        userId: {type: Sequelize.INTEGER},
        storeId: {type: Sequelize.INTEGER}

    });
};
