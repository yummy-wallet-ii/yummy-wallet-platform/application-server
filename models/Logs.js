/**
 * Description: Define Log Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Log = sequelize.define("base_log", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        userId: {type: Sequelize.INTEGER},
        storeId: {type: Sequelize.INTEGER},
        actionId:{type: Sequelize.INTEGER},
        isAdmin:{type: Sequelize.BOOLEAN},
        isConsumer:{type: Sequelize.BOOLEAN},
        typeId: {type: Sequelize.INTEGER},
        message: {type: Sequelize.STRING},
    });

    return Log;
};
