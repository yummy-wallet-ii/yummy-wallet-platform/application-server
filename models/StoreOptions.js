/**
 * Description: Define Store Options Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_store_option", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        logo: {type: Sequelize.STRING},
        customerPercentage: {type: Sequelize.INTEGER},
        yummyPercentage: {type: Sequelize.INTEGER},
        promotedCategory: {type: Sequelize.STRING},
        cashBack: {type: Sequelize.BOOLEAN},
        iBankPay: {type: Sequelize.BOOLEAN},
        stamps: {type: Sequelize.BOOLEAN},
        storePercentage: {type: Sequelize.INTEGER},
        fullAddress: {type: Sequelize.STRING},
        latitude: {type: Sequelize.STRING},
        longitude: {type: Sequelize.STRING},
        frontImage: {type: Sequelize.STRING},
        cashbackPercentage: {type: Sequelize.INTEGER},
        color: {type: Sequelize.STRING},
        geoFenceTitle: {type: Sequelize.STRING},
        geoFenceBody: {type: Sequelize.STRING},
        geoFenceActive: {type: Sequelize.BOOLEAN},
        storeId: {type: Sequelize.INTEGER},
    });
};
