/**
 * Index of Sequelize
 *
 * */
const Sequelize = require("sequelize");
const sequelize = new Sequelize(global.config.database.database, global.config.database.user, global.config.database.password, {
    host: global.config.database.server,
    dialect: global.config.database.dialect,
    operatorsAliases: 0,
    define: {
        scopes: {
            excludeCreatedAtUpdateAt: {
                attributes: {exclude: ['createdAt', 'updatedAt']}
            }
        },
        timestamps: false
    },
    pool: {
        max: global.config.database.pool.max,
        min: global.config.database.pool.min,
        acquire: global.config.database.pool.acquire,
        idle: global.config.database.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.categories = require("./Categories")(sequelize, Sequelize);
db.subCategories = require("./SubCategories")(sequelize, Sequelize);
db.users = require("./User")(sequelize, Sequelize);
db.stores = require("./Store")(sequelize, Sequelize);
db.cities = require("./City")(sequelize, Sequelize);
db.emailLogs = require("./EmailLog")(sequelize, Sequelize);
db.notificationLogs = require("./NotificationLog")(sequelize, Sequelize);
db.countries = require("./Country")(sequelize, Sequelize);
db.requestStatuses = require("./RequestStatus")(sequelize, Sequelize);
db.storeOptions = require("./StoreOptions")(sequelize, Sequelize);
db.storeToUser = require("./StoreToUser")(sequelize, Sequelize);
db.transaction = require("./Transaction")(sequelize, Sequelize);
db.storeRefill = require("./StoreRefill")(sequelize, Sequelize);
db.role = require("./Role")(sequelize, Sequelize);
db.customer = require("./Customer")(sequelize, Sequelize);
db.favourite = require("./Favourite")(sequelize, Sequelize);
db.credits = require("./Credits")(sequelize, Sequelize);
db.settings = require("./Settings")(sequelize, Sequelize);
db.products = require("./Product")(sequelize, Sequelize);
db.logs = require("./Logs")(sequelize, Sequelize);
db.actions = require("./Actions")(sequelize, Sequelize);
db.logTypes = require("./LogType")(sequelize, Sequelize);
db.businessCateogies = require("./BusinessCategories")(sequelize, Sequelize);
db.orders = require("./Orders")(sequelize, Sequelize);
db.ordersDetails = require("./OrderDetails")(sequelize, Sequelize);
db.invoices = require("./Invoice")(sequelize, Sequelize);
db.redeemRequests = require("./RedeemRequest")(sequelize, Sequelize);
db.customerStamps = require("./CustomerStamps")(sequelize, Sequelize);

db.customerStamps.belongsTo(db.categories, {foreignKey: 'categoryId'});


db.orders.hasMany(db.ordersDetails, { foreignKey: 'orderId' });
db.ordersDetails.belongsTo(db.orders, { foreignKey: 'orderId' });

db.customer.hasMany(db.orders, {foreignKey: 'customerId'});
db.orders.belongsTo(db.customer, {foreignKey: 'customerId'});
db.orders.belongsTo(db.stores, {foreignKey: 'storeId'});

db.emailLogs.belongsTo(db.stores, {foreignKey: 'storeId'});
db.emailLogs.belongsTo(db.customer, {foreignKey: 'customerId'});

db.notificationLogs.belongsTo(db.stores, {foreignKey: 'storeId'});
db.notificationLogs.belongsTo(db.customer, {foreignKey: 'customerId'});

db.redeemRequests.belongsTo(db.customer, {foreignKey: 'customerId'});

db.transaction.hasOne(db.customer, {as: 'customer', foreignKey: 'id', sourceKey: 'customerId'});
db.customer.belongsTo(db.transaction);
db.transaction.hasOne(db.stores, {as: 'store', foreignKey: 'id', sourceKey: 'storeId'});
db.stores.belongsTo(db.transaction);

module.exports = db;
