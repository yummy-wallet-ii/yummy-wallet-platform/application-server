/**
 * Description: Define LogType Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const LogType = sequelize.define("base_logType", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        description: {type: Sequelize.STRING},
    });

    return LogType;
};
