/**
 * Description: Define Store Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_store", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        cityId: {type: Sequelize.INTEGER},
        countryId: {type: Sequelize.INTEGER},
        categoryId: {type: Sequelize.INTEGER},
        descriptionEl: {type: Sequelize.STRING},
        descriptionEn: {type: Sequelize.STRING},
        mapPreview: {type:Sequelize.BOOLEAN},
        postalCode: {type: Sequelize.STRING},
        street: {type: Sequelize.STRING},
        email: {type: Sequelize.STRING},
        telephone: {type: Sequelize.STRING},
        website: {type: Sequelize.STRING},
        facebook: {type: Sequelize.STRING},
        instagram: {type: Sequelize.STRING},
        twitter: {type: Sequelize.STRING},
        legalName: {type: Sequelize.STRING},
        headquarters: {type: Sequelize.STRING},
        taxLegalName: {type: Sequelize.STRING},
        taxProfession: {type: Sequelize.STRING},
        taxNumber: {type: Sequelize.STRING},
        taxOffice: {type: Sequelize.STRING},
        vat: {type: Sequelize.INTEGER},
        schedule: {type: Sequelize.TEXT},
        publicKey: {type: Sequelize.STRING},
        secretKey: {type: Sequelize.STRING},
        isActive: {type: Sequelize.BOOLEAN},
        bucket: {type: Sequelize.INTEGER},
        isRegistered: {type: Sequelize.BOOLEAN},
        registrationDate: {type: Sequelize.DATE},
    });
};
