/**
 * Description: Define User Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_user", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        identifier: {
            type: Sequelize.STRING,
            unique: true
        },
        password: {type: Sequelize.STRING},
        provider: {type: Sequelize.STRING},
        roleId: {type:Sequelize.INTEGER}
    });
};
