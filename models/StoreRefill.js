/**
 * Description: Define Bucket To Store Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_store_refill", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        storeId: {type: Sequelize.INTEGER},
        amount: {type: Sequelize.INTEGER},
        transactionIntentId: {type: Sequelize.STRING},
        blockChainTransactionId: {type: Sequelize.STRING},
        transactionDate: {type: Sequelize.DATE}
    });
};
