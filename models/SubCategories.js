/**
 * Description: Define Sub Categories Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_sub_category", {
        name: {
            type: Sequelize.STRING
        }
    });
};
