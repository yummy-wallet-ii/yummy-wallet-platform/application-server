/**
 * Description: Define Invoice Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Invoice = sequelize.define("base_invoice", {
        store_id: {type: Sequelize.INTEGER},
        price: {type: Sequelize.INTEGER},
        price_tax: {type: Sequelize.INTEGER},
        dateCreated: {type: Sequelize.DATE}
    });

    return Invoice;
};
