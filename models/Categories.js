/**
 * Description: Define Category Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("base_category", {
        name: {type: Sequelize.STRING},
        description: {type: Sequelize.STRING},
        storeId: {type: Sequelize.INTEGER},
        image: {type: Sequelize.STRING},
        hasStamps: {type: Sequelize.INTEGER},
        stampsRewards: {type: Sequelize.INTEGER}
    });

    return Category;
};
