/**
 * Description: Define OrderDetails Model
 * @type {{}|{}}
 */
const db = require("./");
const Order = db.orders;
module.exports = (sequelize, Sequelize) => {
    const OrderDetails = sequelize.define('base_orderDetails', {
        productId: {
            type: Sequelize.INTEGER
        },
        productPrice: {
            type: Sequelize.FLOAT
        },
        storeId: {
            type: Sequelize.INTEGER
        },
        comments: {
            type: Sequelize.TEXT
        },
        orderId: {
            type: Sequelize.INTEGER,
            references: {
                model: Order,
                key: 'id'
            }
        }
    }, {
        timestamps: false
    });

    return OrderDetails;
}
