/**
 *
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {

    const Orders = sequelize.define('base_orders', {
        orderUniqueId: {
            type: Sequelize.STRING
        },
        storeId: {
            type: Sequelize.INTEGER
        },
        customerId: {
            type: Sequelize.INTEGER
        },
        status: {
            type: Sequelize.INTEGER
        },
        price: {
            type: Sequelize.FLOAT
        },
        createdAt: {type: Sequelize.DATE},
        completedAt: {type: Sequelize.DATE}
    }, {
        timestamps: false
    })

    return Orders;
}
