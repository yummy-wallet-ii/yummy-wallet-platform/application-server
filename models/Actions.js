/**
 * Description: Define Action Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Action = sequelize.define("base_action", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        description: {type: Sequelize.STRING},
    });

    return Action;
};
