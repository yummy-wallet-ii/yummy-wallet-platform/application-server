/**
 * Description: Define Transaction Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_transaction", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        userId: {type: Sequelize.INTEGER},
        storeId: {type: Sequelize.INTEGER},
        customerId: {type: Sequelize.INTEGER},
        price: {type: Sequelize.DOUBLE},
        store_cashBack: {type: Sequelize.DOUBLE},
        credit_balancer: {type: Sequelize.DOUBLE},
        credits: {type: Sequelize.DOUBLE},
        blockChainTransactionId: {type: Sequelize.STRING},
    });
};
