/**
 * Description: Define Customer Stamps Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_customer_stamps", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        categoryId: {type: Sequelize.INTEGER},
        customerId: {type: Sequelize.INTEGER},
        stamps: {type: Sequelize.INTEGER},
    });
};
