/**
 * Description: Define Request Status Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_request_status", {
        description: {type: Sequelize.STRING}});
};
