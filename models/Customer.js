/**
 * Description: Define Customer Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_customer", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        identifier: {
            type: Sequelize.STRING,
            unique: true
        },
        password: {type: Sequelize.STRING},
        firstName: {type: Sequelize.STRING},
        lastName: {type: Sequelize.STRING},
        provider: {type: Sequelize.STRING},
        mobile: {type: Sequelize.STRING},
        fcmToken: {type: Sequelize.STRING},
        creditCardNumber: {type: Sequelize.STRING},
        IBAN: {type: Sequelize.STRING},
        roleId: {type:Sequelize.INTEGER}
    });
};
