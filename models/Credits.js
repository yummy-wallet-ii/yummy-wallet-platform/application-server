/**
 * Description: Define Credits Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_credit", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        userId: {type: Sequelize.INTEGER},
        credits: {type: Sequelize.DOUBLE}
    });
};
