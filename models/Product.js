/**
 * Description: Define Product Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("base_product", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        description: {type: Sequelize.STRING},
        title: {type: Sequelize.STRING},
        hasDiscount: {type: Sequelize.BOOLEAN},
        categoryId: {type: Sequelize.INTEGER},
        storeId: {type: Sequelize.INTEGER},
        price: {type: Sequelize.FLOAT.UNSIGNED},
        alternativePrice: {type: Sequelize.FLOAT.UNSIGNED},
        image: {type: Sequelize.STRING}
    });

    return Product;
};
