/**
 * Description: Define Email Log Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_email_log", {
        id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
        },
        storeId: {type: Sequelize.INTEGER},
        customerId: {type: Sequelize.INTEGER},
        title: {type: Sequelize.STRING},
        sendDate: {type: Sequelize.DATE},
    });
};
