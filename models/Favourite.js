/**
 * Description: Define Favourites Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_favourite", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        customerId: {type: Sequelize.INTEGER},
        storeId: {type: Sequelize.INTEGER}
    });
};
