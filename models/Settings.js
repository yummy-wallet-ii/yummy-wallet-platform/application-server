/**
 * Description: Define Settings Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    return sequelize.define("base_settings", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        credit_balancer: {type: Sequelize.DOUBLE},
        admin_stripe_public_key: {type: Sequelize.STRING},
        admin_stripe_private_key: {type: Sequelize.STRING},
    });
};
