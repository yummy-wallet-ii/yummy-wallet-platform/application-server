/**
 * Description: Define Business Category Model
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const BusinessCategory = sequelize.define("base_business_category", {
        description: {type: Sequelize.STRING},
    });

    return BusinessCategory;
};
